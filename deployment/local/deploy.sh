#!/usr/bin/env bash
set -euxo pipefail errexit

minikube start --force --driver=docker --alsologtostderr -v=7 --docker-env="ENVIRONMENT=local"
eval "$(minikube -p minikube docker-env)"
IMAGE_APP_TAG=$(date +%s)
IMAGE_NGINX_TAG=$(date +%s)
export IMAGE_TAG
export IMAGE_NGINX_TAG
export IMAGE_APP="web-store:app${IMAGE_APP_TAG}"
export IMAGE_NGINX="web-store:nginx${IMAGE_NGINX_TAG}"

docker build --target=app --tag "${IMAGE_APP}" -f deployment/Dockerfile  .
docker tag "${IMAGE_APP}" "grinkoevgenii/${IMAGE_APP}"

docker build --target=nginx --tag "${IMAGE_NGINX}" -f deployment/Dockerfile  .
docker tag "${IMAGE_NGINX}" "grinkoevgenii/${IMAGE_NGINX}"

docker push "docker.io/grinkoevgenii/${IMAGE_APP}"
docker push "docker.io/grinkoevgenii/${IMAGE_NGINX}"

helm lint \
  --set env=local \
  deployment/.helm \
  -f deployment/.helm/values.yaml \
  -f deployment/.helm/values_configmap.yaml \
  --set image.tag="grinkoevgenii/${IMAGE_APP}" \
  --set image.scheduler="grinkoevgenii/${IMAGE_APP}" \
  --set image.nginx="grinkoevgenii/${IMAGE_NGINX}" \
  --set image.repository="grinkoevgenii/web-store" \
  --set configmap.local.DB_PASSWORD="root" \
  --set configmap.local.DB_DATABASE="web-store-db"

helm upgrade -f deployment/.helm/templates/secrets/docker.yaml -f deployment/.helm/values.yaml -f deployment/.helm/values_configmap.yaml -i app deployment/.helm \
  --set env=local \
  --set image.tag="grinkoevgenii/${IMAGE_APP}" \
  --set image.scheduler="grinkoevgenii/${IMAGE_APP}" \
  --set image.nginx="grinkoevgenii/${IMAGE_NGINX}" \
  --set image.repository="grinkoevgenii/web-store" \
  --set image.repository="grinkoevgenii/web-store" \
  --set configmap.local.DB_PASSWORD="root" \
  --set configmap.local.DB_DATABASE="web-store-db"

