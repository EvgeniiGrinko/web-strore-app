#!/usr/bin/env sh

cp /var/www/deployment/php/www.conf /usr/local/etc/php-fpm.d/www.conf

CRONTAB="*/1 */1 */1 */1 */1 cd /var/www/src && php artisan schedule:run > /proc/1/fd/1 2>&1"
echo $CRONTAB >> /etc/crontabs/root
crond

php-fpm