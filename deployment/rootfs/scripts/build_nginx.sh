#!/usr/bin/env bash

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build --progress=plain --target=nginx --tag ${NGINX_IMAGE} -f deployment/Dockerfile .
docker push ${NGINX_IMAGE}
docker rmi ${NGINX_IMAGE}
