#!/bin/bash
set -euo pipefail errexit

# Installing additional tools
apk add curl
apk add bash
apk add go
apk add git
apk add openssl

# Installing Kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin \

chmod 0600 -R /builds/${CI_PROJECT_ROOT_NAMESPACE}

# Installing Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod +x get_helm.sh
./get_helm.sh

kubectl config use-context EvgeniiGrinko/web-store:web-store-agent

apk add helm

# Linting helm chart alongside with repo-based values
helm lint deployment/.helm \
  -f deployment/.helm/values.yaml \
  -f deployment/.helm/values_configmap.yaml \
  --set env=production \
  --set image.tag="grinkoevgenii/${IMAGE_APP}" \
  --set image.scheduler="grinkoevgenii/${IMAGE_APP}" \
  --set image.nginx="grinkoevgenii/${IMAGE_APP}" \
  --set configmap.production.DB_PASSWORD="${MYSQL_PASSWORD}" \
  --set configmap.production.DB_DATABASE="${DB_DATABASE}" \
  --set configmap.production.TELEGRAM_BOT_TOKEN="${TELEGRAM_BOT_TOKEN}" \
  --set configmap.production.CHAT_ID_TO_REPORT_BY_TELEGRAM_BOT="${CHAT_ID_TO_REPORT_BY_TELEGRAM_BOT}" \
  --set configmap.production.FREEDOMPAY_MERCHANT_SECRET_PAYOUT="${FREEDOMPAY_MERCHANT_SECRET_PAYOUT}" \
  --set configmap.production.FREEDOMPAY_MERCHANT_SECRET_PAYIN="${FREEDOMPAY_MERCHANT_SECRET_PAYIN}" \
  --set configmap.production.REDIS_PASSWORD="${REDIS_PASSWORD}"

helm repo add jetstack https://charts.jetstack.io
helm repo update
#
#helm install cert-manager jetstack/cert-manager --version 1.8.0 \
#  --namespace cert-manager --create-namespace \
#  --set global.leaderElection.namespace=cert-manager

helm upgrade --install cert-manager jetstack/cert-manager --version 1.8.0   --namespace cert-manager --create-namespace   --set global.leaderElection.namespace=cert-manager

helm upgrade --install ${CI_PROJECT_NAME} deployment/.helm \
  -f deployment/.helm/values.yaml \
  -f deployment/.helm/values_configmap.yaml \
  --set env=production \
  --set image.tag="grinkoevgenii/${IMAGE_APP}" \
  --set image.scheduler="grinkoevgenii/${IMAGE_APP}" \
  --set image.nginx="grinkoevgenii/${IMAGE_NGINX}" \
  --set configmap.production.DB_PASSWORD="${MYSQL_PASSWORD}" \
  --set configmap.production.DB_DATABASE="${DB_DATABASE}" \
  --set configmap.production.TELEGRAM_BOT_TOKEN="${TELEGRAM_BOT_TOKEN}" \
  --set configmap.production.CHAT_ID_TO_REPORT_BY_TELEGRAM_BOT="${CHAT_ID_TO_REPORT_BY_TELEGRAM_BOT}" \
  --set configmap.production.FREEDOMPAY_MERCHANT_SECRET_PAYOUT="${FREEDOMPAY_MERCHANT_SECRET_PAYOUT}" \
  --set configmap.production.FREEDOMPAY_MERCHANT_SECRET_PAYIN="${FREEDOMPAY_MERCHANT_SECRET_PAYIN}" \
  --set configmap.production.REDIS_PASSWORD="${REDIS_PASSWORD}"

