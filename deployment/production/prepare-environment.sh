#!/usr/bin/env bash
set -euxo pipefail errexit

IMAGE_APP_TAG="${CI_COMMIT_SHA}"
IMAGE_NGINX_TAG="${CI_COMMIT_SHA}"
export IMAGE_TAG
export IMAGE_NGINX_TAG
export IMAGE_APP="web-store:app${IMAGE_APP_TAG}"
export IMAGE_NGINX="web-store:nginx${IMAGE_NGINX_TAG}"