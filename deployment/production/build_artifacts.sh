#!/usr/bin/env bash

apk add git

docker system prune -f
docker image prune -a --force --filter "until=2h"

docker build --target=app --tag "${IMAGE_APP}" -f deployment/Dockerfile  .
docker tag "${IMAGE_APP}" "grinkoevgenii/${IMAGE_APP}"
docker build --target=nginx --tag "${IMAGE_NGINX}" -f deployment/Dockerfile  .
docker tag "${IMAGE_NGINX}" "grinkoevgenii/${IMAGE_NGINX}"

docker login -u="${DOCKER_HUB_LOGIN}" -p="${DOCKER_HUB_PASSWORD}"
docker push "grinkoevgenii/${IMAGE_APP}"
docker push "grinkoevgenii/${IMAGE_NGINX}"

docker rmi ${IMAGE_NGINX}
docker rmi ${IMAGE_APP}