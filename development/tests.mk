tests-phpstan:
	docker compose run --rm app sh -c "vendor/bin/phpstan analyse -c tests/PHPStan/phpstan.neon --memory-limit=2G"

tests-phpcs:
	docker compose run --rm app sh -c "vendor/bin/php-cs-fixer check --format=txt --allow-risky=yes --diff"

tests-phpcs-fix:
	docker compose run --rm app sh -c "vendor/bin/php-cs-fixer fix --format=txt --allow-risky=yes"

helm-render-production:
	helm template deployment/.helm/ \
	  -f deployment/.helm/values.yaml \
	  -f deployment/.helm/values_configmap.yaml \
	  --set env=production