minikube-start:
	chmod u+x deployment/local/deploy.sh \
	&& ./deployment/local/deploy.sh

minikube-stop:
	minikube stop

#expose-app
minikube-ea:
	minikube service app-svc -n def --url

#expose-minio-ui
minikube-emui:
	minikube service minio-svc -n def --url