compose-build:
	docker compose build

compose-pull:
	docker compose pull

compose-container-app-create:
	docker compose run --rm app sh -c "composer install"

compose-up:
	docker compose up -d

compose-stop:
	docker compose stop

compose-in-app:
	docker compose exec app sh -l

compose-down:
	docker compose down

#down with prune of volumes
compose-down-v:
	docker compose down -v

compose-db-create:
	docker compose run --rm app php artisan migrate:install

compose-db-init:
	docker compose run --rm app php artisan migrate:refresh --force
	docker compose run --rm app php artisan db:seed --force

compose-db-migrate:
	docker compose run --rm app php artisan migrate

compose-vite:
	docker compose run --rm app npm i
	docker compose run --rm app npm run build

compose-sh-nginx:
	docker compose run --rm nginx sh

compose-sh-app:
	docker compose run --rm app sh
