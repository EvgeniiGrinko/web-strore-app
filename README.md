## Web-Store

## Disclaimer

This project is not a production ready application (but can be a good starting point for it).

Main purpose of it is to learn the concepts of technologies that I reckon maybe worth studying and using.

## Tech Stack:

- PHP 8.3
- Laravel 10.39
- Docker Compose (Docker Hub as image repository)
- Kubernetes
- Minio (AWS S3 compatible storage)
- React 18.2.0 + Vite
- Inertia (SSR)
- Gitlab CI/CD
- OpenAPI (Swagger)
- Kafka
- Nginx
- MySQL

Project page: http://localhost  

# Before start create env file: 
```shell
cp src/.env.example .env
```

# Available commands
```shell
## Work with project
make compose-build      # Build project
make compose-db-create  # Create database 
make compose-db-init    # (Re)Start Database
make compose-db-migrate # Run migrations
make compose-vite       # Build frontend

make compose-up         # Start project
make compose-down       # Stop project

make compose-sh-nginx   # Get into nginx container
make compose-sh-app     # Get into app container

## Kubernetes (Minikube)
make minikube-start     # Start project
On first start you probably need to get into the pod, maybe using Lens, and run following commands:
php artisan migrate:install
php artisan migrate
php artisan db:seed

make minikube-stop      # Stop project
make minikube-ea        # Expose app
make minikube-emui      # Expose Minio

## Tests
make tests-phpstan      # Start PHPStan analysis
make tests-phpcs        # Check code style
make tests-phpcs-fix    # Fix code style

## Check helm rendering
make helm-render-production

```

## Debugging

In this project I use two main tools:
- XDebug 3 (to turn it on uncomment line in Dockerfile to introduce local configurations: "...php/conf.d/local.ini")
- Telescope (proceed to page: http://localhost/telescope)


## Contacts and Additional Info

The inspiration for creating this project was derived from Roman Davydov's
[An online store on Laravel from scratch](https://youtube.com/playlist?list=PL5RABzpdpqAlSRJS1KExmJsaPFQc161Dy&si=HhZJvi_ZLyROkdIr)

If you have any questions, feel free to contact me on
[LinkedIn](www.linkedin.com/in/evgenii-grinko)
