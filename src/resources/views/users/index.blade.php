@extends('master')

@section('title', 'Страница User №' . $user->name)

@section('content')
    <div class="py-4">
        <div class="container">
            <div class="justify-content-center">
                <div class="panel">
                    <h1>Пользователь №{{ $user->id }}</h1>
                    <p>Имя <b>{{ $user->name }}</b></p>
                    <p>Номер телефона <b>{{ $user->phone_number }}</b></p>
                    <p>Список заказов </p>

                    <table class="table">
                        <tbody>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Когда создан
                            </th>
                            <th>
                                Сумма
                            </th>
                            <th>
                                Действия
                            </th>
                        </tr>
                        @foreach($user->orders as $order)
                            <tr>
                                <td>{{ $order->id}}</td>
                                <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                                <td>{{ $order->merchant_trans_amount}} {{ $order->merchant_currency}}</td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('orders.show', $order)}}"
                                        >Открыть</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
