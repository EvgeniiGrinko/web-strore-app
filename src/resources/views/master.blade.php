<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="9974e0eee0f21447"/>
    <title>Интернет-магазин: @yield('title') </title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div id="navbar" class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{route('index')}}">Главная</a>
{{--            <a class="navbar-brand" href="{{route('check')}}">Проверить платеж</a>--}}
            @if(Auth::check() && Auth::user()->isAdmin())
                <a class="navbar-brand" href="{{route('freedom-payout-view')}}">Выплата</a>
            @endif
            <a class="navbar-brand fixed-right-side" href="{{route('basket')}}">В корзину</a>

            <a class="navbar-brand" href="{{route('seed')}}">Seed</a>

        @guest
                <a class="navbar-brand" href="{{ route('login') }}">Войти</a>
                <a class="navbar-brand" href="{{ route('register') }}">Зарегистрироваться</a>
            @endguest
            @auth
                <a class="navbar-brand" href="{{ route('logout')}}">
                    Выйти
                </a>
                @if(Auth::check() && Auth::user()->isAdmin())
                    <a class="navbar-brand" href="{{route('orders.index')}}">Панель администратора</a>
                @endif
                <a class="navbar-brand" href="{{route('user', [Auth::user()->id])}}">Вы вошли
                    как {{Auth::user()->name}}</a>
            @endauth
        </div>
    </div>
</nav>
<div class='container'>
    <div class="starter-template">
        @if (session()->has('success'))
            <p class="alert alert-success">{{session()->get('success')}}</p>
        @endif
        @if (session()->has('warning'))
            <p class="alert alert-warning">{{session()->get('warning')}}</p>
        @endif
        @yield('content')
    </div>
</div>
<footer>
</footer>
</body>
{{--<script src="main.js"></script>--}}

</html>
