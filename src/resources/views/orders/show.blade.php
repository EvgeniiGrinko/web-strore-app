@extends('master')

@section('title', 'Заказ №' . $order->id)

@section('content')
    <div class="py-4">
        <div class="container">
            <div class="justify-content-center">
                <div class="panel">
                    <h1>Заказ №{{ $order->id }}</h1>
                    <p>Заказчик: <b>{{ $order->customer_fname }}</b></p>
                    <p>Номер телефона: <b>{{ $order->phone_number }}</b></p>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название товара (услуги)</th>
                            <th>Кол-во</th>
                            <th>Цена</th>
                            <th>Размер НДС</th>
                            <th>Цена со скидкой</th>
                            <th>Стоимость со скидкой</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>
                                    <a href="{{ route('product.show', [$product->id]) }}">
                                        <img width="50px" height="50px"
                                             src="{{ Vite::asset($product->image) }}">
                                        {{ $product->name }}
                                    </a>
                                </td>
                                <td><span class="badge"> {{ $product->pivot->count}}</span></td>
                                <td>{{ $product->price }} {{ $order->merchant_currency}}</td>
                                <td>{{ $product->vat_percent }}%</td>
                                @if(!empty($product->pivot->discount_price))
                                    <td>{{ $product->pivot->count * $product->pivot->discount_price}} {{ $order->merchant_currency}}</td>
                                    <td>{{ $product->pivot->count * $product->pivot->discount_price}} {{ $order->merchant_currency}}</td>
                                @else
                                    <td>Без скидки</td>
                                    <td>Без скидки</td>

                                @endif
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="5">Всего (без скидки):</td>
                            <td>{{ $order->getFullSum() }} {{ $order->merchant_currency}}</td>
                        </tr>
                        @if(!empty($product->pivot->discount_price))
                        <tr>
                            <td colspan="5">Всего (со скидкой):</td>
                            <td>{{ $order->merchant_trans_amount }} {{ $order->merchant_currency}}</td>
                        </tr>
                        @else
                            <tr>
                                <td colspan="5">Всего (со скидкой):</td>
                                <td>Без скидки</td>
                            </tr>
                            @endif
                        <tr>
                            <td colspan="5">Статус заказа:</td>
                            <td>{{ $order->getCurrentStatusDescription() }}</td>
                        </tr>
                        <tr>
                            <td colspan="5">Статус оплаты:</td>
                            <td>{{$order->getCurrentPaymentStatus()}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
            </div>
            <div>
                @if($order->payment_state === 0 || $order->payment_state === 1)
                    <form action="{{ route('pay', $order->id) }}" method="POST">
                        @csrf
                        <div style="display:flex; justify-content: right">
                            <button id="buttonPay" class="btn btn-success" style="margin-right: 100px"
                                    type="submit">Оплатить заказ
                            </button>
                        </div>
                    </form>

                    <div class="popup" data-modal id="popup">
                        <div class="popup_dialog">
                            <div class="popup_content text-center">
                                <button type="button" class="popup_close" id="popup_close_button"
                                        onclick="closeButton({{$order->id}})"><strong>&times;</strong></button>
                                <div class="popup_form" id="popup_form">
                                    <form class="form">
                                        <h2>Олатите заказ</h2>
                                        <iframe id="iframe" src="" width="570" height="550" NORESIZE>Ваш браузер не поддерживает
                                            фреймы!
                                        </iframe>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
