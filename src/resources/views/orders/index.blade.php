@extends('master')

@section('title', 'Заказы')

@section('content')
    <div class="col-md-12">
        <h1>Заказы</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Имя Заказчика
                </th>
                <th>
                    Телефон Заказчика
                </th>
                <th>
                    Когда создан
                </th>
                <th>
                    Сумма
                </th>
                <th>
                    Действия
                </th>
            </tr>

            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id}}</td>
                    <td>{{ $order->customer_fname }}</td>
                    <td>{{ $order->phone_number }}</td>
                    <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                    <td>{{ $order->merchant_trans_amount}} {{ $order->merchant_currency}}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <a class="btn btn-success" type="button"
                               href="{{ route('orders.show', $order)}}"
                            >Открыть</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    {{ $orders->links() }}
@endsection
