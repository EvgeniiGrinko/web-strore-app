export default function ApplicationLogo() {
    return (
        <img src="https://flowbite.com/docs/images/logo.svg" className="h-8" alt="Flowbite Logo"/>
    );
}
