import { Head, Link } from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

export default function PaymentPage({ order, products, fullSum, fullSumWthDiscount, currencyCode }) {

    function getImageUrl(name) {
        return new URL(`/${name}`, import.meta.url).href
    }

    return (
        <>
            <Head title="Web Store - Payment Page" />
            <div className="min-h-screen flex flex-col">
                <Header />
                <main className="flex-grow">
                    <div className="relative overflow-x-auto shadow-md sm:rounded-lg m-10">
                        <h1>
                            Order Created Successfully № {`${order.id}`}
                        </h1>
                        <p>
                            Customer: <b>{`${order.customer_fname}`}</b>
                        </p>
                        <p>
                            Telephone Number: <b>{`${order.phone_number}`}</b>
                        </p>
                        <table className="w-full text-sm text-center rtl:text-right text-gray-500 dark:text-gray-400">
                            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" className="px-6 py-3">
                                    Product
                                </th>
                                <th scope="col" className="px-6 py-3 content-center">
                                    Qty
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Price
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Tax
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Price with Discount
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Discount
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Total with Discount
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {Object.keys(products).map(function (productId, keyName) {
                                const discountedProduct = products[productId].pivot.discount_price;
                                let discount;
                                if (discountedProduct) {
                                    discount = <td>
                                        {`${products[productId].pivot.count * products[productId].pivot.discount_price}`} {currencyCode}
                                    </td>
                                } else {
                                    discount = <td>Without Discount</td>;
                                }

                                return (
                                    <tr key={keyName}
                                        className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                        <td className="p-4">
                                            <Link href={route('product.show', `${products[productId]}`)}>
                                                <img
                                                    className="w-16 md:w-32 max-w-full max-h-full"
                                                    src={getImageUrl(`${products[productId].image}`)}
                                                    alt={`${products[productId].name}`}
                                                />
                                                {`${products[productId].name}`}
                                            </Link>
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            <span className="badge">
                                                {`${products[productId].pivot.count}`}
                                            </span>
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            {`${products[productId].price}`} {currencyCode}
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            {`${products[productId].vat_percent}`} %
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            {parseFloat(`${products[productId].pivot.discount_price}`).toFixed(2)} {currencyCode}
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            {parseFloat(`${(products[productId].price - products[productId].pivot.discount_price) ?? ''}`)} {discount ? currencyCode : ''}
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                            {parseFloat(`${products[productId].pivot.discount_price * products[productId].pivot.count}`).toFixed(2)} {currencyCode}
                                        </td>
                                    </tr>
                                );
                            })}
                            <tr>
                                <td className="px-6 py-3">
                                    Total (without discount)
                                </td>
                                <td className="px-6 py-3">
                                    {parseFloat(`${fullSum}`).toFixed(2)} {currencyCode}
                                </td>
                                <td className="px-6 py-3">
                                    Total (with discount)
                                </td>
                                <td className="px-6 py-3">
                                    {parseFloat(`${fullSumWthDiscount}`).toFixed(2)} {currencyCode}
                                </td>
                                <td className="px-6 py-3">
                                    To be paid:
                                </td>
                                <td className="px-6 py-3">
                                    {parseFloat(`${fullSumWthDiscount}`).toFixed(2)} {currencyCode}
                                </td>
                                <td className="px-6 py-3">
                                    <form action={route('pay', `${order.id}`)} method="POST">
                                        <button type="submit"
                                                className="relative inline-flex items-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-cyan-500 to-blue-500 group-hover:from-cyan-500 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-cyan-200 dark:focus:ring-cyan-800">
                                    <span
                                        className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                                        Pay
                                    </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </main>
                <Footer />
            </div>
        </>
    );
}
