import {Head} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import AddToCartButton from "@/Pages/Components/AddToCartButton.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

function getImageUrl(name) {
    return new URL(`/${name}`, import.meta.url).href
}
export default function Product({ product }) {
    const logo = getImageUrl(product.image);
    return (
        <>
            <Head title={product.name} />
            <Header/>
            <body>
                <div className="relative sm:flex sm:justify-center sm:items-center flex-col m-3 min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">
                        <img className="rounded-lg max-h-96" src={logo} alt={product.name}/>
                        <h1 className="mt-6 text-xl font-semibold text-gray-900 dark:text-white">
                            {product.name}
                        </h1>
                        <h2 className="mt-5 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                            {product.description}
                        </h2>
                        <h1 className="mt-5 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                            Price: {product.price}
                        </h1>
                        <h1 className="mt-5 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                            In stock: {product.count}
                        </h1>
                        <AddToCartButton product={product}/>
                </div>
            </body>
            <Footer/>

            <style>{`
                .bg-dots-darker {
                    background-image: url("data:image/svg+xml,%3Csvg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.22676 0C1.91374 0 2.45351 0.539773 2.45351 1.22676C2.45351 1.91374 1.91374 2.45351 1.22676 2.45351C0.539773 2.45351 0 1.91374 0 1.22676C0 0.539773 0.539773 0 1.22676 0Z' fill='rgba(0,0,0,0.07)'/%3E%3C/svg%3E");
                }
                @media (prefers-color-scheme: dark) {
                    .dark\\:bg-dots-lighter {
                        background-image: url("data:image/svg+xml,%3Csvg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.22676 0C1.91374 0 2.45351 0.539773 2.45351 1.22676C2.45351 1.91374 1.91374 2.45351 1.22676 2.45351C0.539773 2.45351 0 1.91374 0 1.22676C0 0.539773 0.539773 0 1.22676 0Z' fill='rgba(255,255,255,0.07)'/%3E%3C/svg%3E");
                    }
                }
            `}</style>
        </>
    );
}
