import { Link, useForm } from '@inertiajs/react';

function getImageUrl(name) {
    return new URL(`/${name}`, import.meta.url).href;
}

export default function ProductCard({ product, currencyCode }) {
    const logo = getImageUrl(product.image);

    const { post } = useForm({})

    const submit = (e) => {
        e.preventDefault();
        post(route('basket.add', product.id));
    };

    return (
        <>
            <section className="m-4 flex flex-col items-center justify-center text-center">  {/* Increase the margin around the card */}
                <Link
                    href={route('product.show', product.id)}
                    className="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500"
                >
                    <img className="rounded-lg max-h-60" src={logo} alt={product.name} />
                </Link>
                <Link
                    href={route('product.show', product.id)}
                    className="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500"
                >
                    <h4 className="mt-1 text-lg font-medium text-gray-900 dark:text-white">  {/* Reduce margin-top to 1 */}
                        {product.name}
                    </h4>
                </Link>
                <h5 className="mt-1 text-lg font-medium text-gray-900 dark:text-white">  {/* Reduce margin-top to 1 */}
                    {product.price} {currencyCode}
                </h5>
                <form onSubmit={submit} className="mt-2">  {/* Reduce margin-top to 2 */}
                    <button
                        className="relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-xs font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-teal-300 to-lime-300 group-hover:from-teal-300 group-hover:to-lime-300 dark:text-white dark:hover:text-gray-900 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-lime-800">
                        <span
                            className="relative px-3 py-1 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                            Add to Cart
                        </span>
                    </button>
                </form>
            </section>
        </>
    );
}
