import {Link} from "@inertiajs/react";

export default function Paginator(responseData) {
   const  {current_page, data, first_page_url, from, last_page, last_page_url, links, next_page_url, path, per_page, prev_page_url, to, total} = responseData;
    const linksData = [];

    links.forEach((link) => {
        const ariaCurrent = link.active ? 'page' : null;
        if (!link.url) {
            return;
        }

        if (link.label.includes('&laquo; Previous')) {
            return;
        }

        if (link.label.includes('Next &raquo;')) {
            return;
        }

        linksData.push(
            <li>
                <Link href={link.url}
                      aria-current={ariaCurrent}
                      className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                    {link.label}
                </Link>
            </li>
        );

    });

    return (
        <>
            <div className="flex flex-col items-center m-1">
                <span className="text-sm text-gray-700 dark:text-gray-400">
                    Showing
                    <span className="font-semibold text-gray-900 dark:text-white"> {from} </span>
                     to
                    <span className="font-semibold text-gray-900 dark:text-white"> {to} </span>
                     of
                    <span
                        className="font-semibold text-gray-900 dark:text-white"> {total} </span>
                     Entries
                </span>
            </div>
            <nav aria-label="Paginator" className="flex justify-center">
                <ul className="flex items-center -space-x-px h-8 text-sm">
                    {prev_page_url ? (
                        <li>
                            <Link href={prev_page_url}
                                  className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                <span className="sr-only">Previous</span>
                                <svg className="w-2.5 h-2.5 rtl:rotate-180" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" d="M5 1 1 5l4 4"/>
                                </svg>
                            </Link>
                        </li>
                    ) : (<></>)}
                    {linksData}
                    {next_page_url ? (
                        <li>
                            <Link href={next_page_url}
                                  className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                <span className="sr-only">Next</span>
                                <svg className="w-2.5 h-2.5 rtl:rotate-180" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" d="m1 9 4-4-4-4"/>
                                </svg>
                            </Link>
                        </li>
                    ) : (<></>)}
                </ul>
            </nav>
        </>
    );
}
