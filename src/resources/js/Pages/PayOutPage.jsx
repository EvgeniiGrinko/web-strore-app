import {Head} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

export default function PayOutPage() {

    function getLink() {
        // Get the text field
        const copyText = document.getElementById("payout_link");

        const myEvent = new PointerEvent('pointerdown')

        copyText.dispatchEvent(myEvent);

    }

    document.addEventListener('DOMContentLoaded',function () {
        if (navigator && navigator.clipboard && navigator.clipboard.writeText && document.getElementById('copy_container')) {
            document.getElementById('copy_container').innerHTML = '<img id="copyId2"  onclick="getLink()" height=\'15px\' src="/images/copy-icon.svg">\n'
        }
    });

    function sendReqForPayout() {
        const payout_sum = document.getElementById("payout_sum").value;
        // const token = document.getElementById("_token").value;
        const token = document.querySelector('meta[name="csrf-token"]').content;
        if (!payout_sum) {
            return;
        }
        document.getElementById("payout-link-group").style.display = 'none';
        document.getElementById("payout_link").innerHTML = '';
        document.getElementById("payout_link").href = '';
        document.getElementById("errorPayoutLink").style.display = 'none';
        document.getElementById("errorPayoutLink").innerHTML = '';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax(
            {
                type: "POST",
                async: true,
                url: "/payout",
                data: {payout_sum: payout_sum, token: token},
                dataType: 'json',
            }
        ).done(function (msg) {
            if (msg.pg_redirect_url)
            {
                document.getElementById("payout-link-group").style.display = '';
                document.getElementById("payout_link").innerHTML = msg.pg_redirect_url;
                document.getElementById("payout_link").href = msg.pg_redirect_url;
            } else if (msg.error)
            {
                console.log(msg.error);
                document.getElementById("errorPayoutLink").style.display = '';
                document.getElementById("errorPayoutLink").innerHTML = msg.error;
            }

        });
    }


    return (<>
        <Head title="Web Store - PayOut"/>
        <Header/>
            <body>
                <div className="center">
                    <div className="col-lg-6">
                        <br/>
                        <p id="status">Enter Sum For PayOut</p>
                        <br/>
                        <div className="input-group">
                            <input id="payout_sum" type="number" className="form-control" name="payout_sum" required
                                   placeholder="100"/>
                            <span className="input-group-btn">
                            <button className="btn btn-default" type="submit" onClick={sendReqForPayout()}>Go!</button>
                        </span>
                        </div>
                    </div>
                </div>
                <div className="center">
                    <div className="col-lg-6">
                        <p id="errorPayoutLink"></p>
                        <div id="payout-link-group" style="display: none;">
                            <div className="copy">
                                <div id="copy_container"></div>
                                <br/>
                                <p>Payout Link</p>
                                <a href="" target="_blank" id="payout_link"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        <Footer/>
    </>);
}
