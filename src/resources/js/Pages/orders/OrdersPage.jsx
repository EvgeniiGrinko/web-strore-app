import {Head} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";
import Paginator from "@/Pages/Components/Paginator.jsx";

export default function OrdersPage({orders}) {

    return (<>
        <Head title="Web Store - Orders"/>
        <Header/>
        <body>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg m-10">
                <h1>
                    Order Created Successfully № {`${order.id}`}
                </h1>
                <p>
                    Customer: <b>{`${order.customer_fname}`}</b>
                </p>
                <p>
                    Telephone Number: <b>{`${order.phone_number}`}</b>
                </p>
                <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Order Number
                        </th>
                        <th scope="col" className="px-6 py-3 content-center">
                            Client Name
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Client Phone Number
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Creation Date
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Sum
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(orders).map(function (orderId, keyName) {
                        const discountedProduct = orders[orderId].pivot.discount_price;
                        let discount;
                        if (discountedProduct) {
                            discount = <td>
                                {`${orders[orderId].pivot.count * orders[orderId].pivot.discount_price}`} {`${order.merchant_currency}`}
                            </td>
                        } else {
                            discount = <td>Without Discount</td>;
                        }

                        return (<>
                            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <td className="p-4">
                                    {`${orders[orderId].id}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].customer_fname}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].phone_number}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].created_at}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].merchant_trans_amount}`} {`${orders[orderId].merchant_currency}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    <div className="btn-group" role="group">
                                        <a className="btn btn-success" type="button" href={route('orders.show', `${order.id}`)}>
                                            Open
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </>)
                    })}
                    </tbody>
                </table>
            </div>
            <Paginator/>
        {/*{{ $orders->links() }}*/}
        </body>
        <Footer/>
    </>);
}
