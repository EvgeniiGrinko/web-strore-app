import {Head} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

export default function OrderPage({order, products, full_sum, status_description, payment_status}) {

    function getImageUrl(name) {
        return new URL(`/${name}`, import.meta.url).href
    }

    function closeButton(orderId){
        // if(confirm("Do you want to close this popup?\nEither OK or Cancel.")){
        document.getElementById("popup").style.display = 'none';
        let statusReceived = null;
        const intervalId = setTimeout(function () {

            $.ajax(
                {
                    type: "POST",
                    async: true,
                    url: "/merchant-transaction-check",
                    data: {
                        order_id: orderId,
                        check: 'After payment'
                    },
                    dataType: 'json',
                }
            ).done(function (msg) {
                document.getElementById('payment-state').innerText = msg.payment_state === 2 ? 'Оплачен' : msg.payment_state === 3 ? 'Отменен' : 'Не оплачен';
                document.getElementById('payment-type').setAttribute('disabled', '');
                statusReceived = msg.transaction_type;
            })

        }, 2000);
    }

    let payButton = ''

    if(order.payment_state === 0 || order.payment_state === 1) {
        payButton = <>
            <form action={route('pay', `${order.id}`)} method="POST">
            <button type="submit"
                    className="relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-cyan-500 to-blue-500 group-hover:from-cyan-500 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-cyan-200 dark:focus:ring-cyan-800">
                <span
                    className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                    Pay
                </span>
            </button>
            </form>
            <div className="popup" data-modal id="popup">
                <div className="popup_dialog">
                    <div className="popup_content text-center">
                        <button type="button" className="popup_close" id="popup_close_button"
                                onClick={closeButton(order.id)}><strong>&times;</strong>
                        </button>
                        <div className="popup_form" id="popup_form">
                            <form className="form">
                                <h2>Олатите заказ</h2>
                                <iframe id="iframe" src="" width="570" height="550" NORESIZE>
                                    Your browser doesn't support frames
                                </iframe>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }

    return (<>
        <Head title="Web Store - Order"/>
        <Header/>
        <body>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg m-10">
                <h1>
                    Order № {`${order.id}`}
                </h1>
                <p>
                    Customer Name: <b>{`${order.customer_fname}`}</b>
                </p>
                <p>
                    Telephone Number: <b>{`${order.phone_number}`}</b>
                </p>
                <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Product
                        </th>
                        <th scope="col" className="px-6 py-3 content-center">
                            Qty
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Price
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Tax
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Price with Discount
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Total with Discount
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(products).map(function (productId, keyName) {
                        const discountedProduct = products[productId].pivot.discount_price;
                        let discount;
                        if (discountedProduct) {
                            discount = <td>
                                {`${products[productId].pivot.count * products[productId].pivot.discount_price}`} {`${order.merchant_currency}`}
                            </td>
                        } else {
                            discount = <td>Without Discount</td>;
                        }

                        return (<>
                            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <td className="p-4">
                                    <a href={route('product.show', `${productId}`)}>
                                        <img
                                            className="w-16 md:w-32 max-w-full max-h-full"
                                            src={getImageUrl(`${products[productId].image}`)}
                                            alt={`${products[productId].name}`}
                                        />
                                        {`${products[productId].name}`}
                                    </a>
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                        <span className="badge">
                                            {`${products[productId].pivot.count}`}
                                        </span>
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${products[productId].price}`} {`${order.merchant_currency}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${products[productId].vat_percent}`} %
                                </td>
                                {discount}
                            </tr>
                        </>)
                    })}
                    <tr>
                        <td className="px-6 py-3">
                            Total (without discount)
                        </td>
                        <td className="px-6 py-3">
                            {parseFloat(`${full_sum}`).toFixed(2)}
                            {`${order.merchant_currency}`}
                        </td>
                        <td className="px-6 py-3">
                            Total (with discount)
                        </td>
                        <td className="px-6 py-3">
                            {parseFloat(`${full_sum_wth_discount}`).toFixed(2)}
                            {`${order.merchant_currency}`}
                        </td>
                        <td className="px-6 py-3">
                            To be paid:
                        </td>
                        <td className="px-6 py-3">
                            {parseFloat(`${full_sum_wth_discount}`).toFixed(2)}
                            {`${order.merchant_currency}`}
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="5">Order Status:</td>
                        <td>{`${status_description}`}</td>
                    </tr>
                    <tr>
                        <td colSpan="5">Payment Status:</td>
                        <td>{`${payment_status}`}</td>
                    </tr>
                    </tbody>
                </table>
                <div>
                    {`${payButton}`}
                </div>
            </div>
        </body>
        <Footer/>
    </>);
}
