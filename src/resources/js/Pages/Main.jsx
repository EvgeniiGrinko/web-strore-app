import { Head, useForm } from '@inertiajs/react';
import ProductCard from "@/Pages/Components/ProductCard.jsx";
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";
import Paginator from "@/Pages/Components/Paginator.jsx";

export default function Main({ products, currencyCode }) {
    const rows = [];
    const queryParams = new URLSearchParams(window.location.search);
    const { data, setData, get } = useForm({
        priceFrom: queryParams.get("priceFrom"),
        priceTo: queryParams.get("priceTo"),
        productName: queryParams.get("productName")
    });

    const handleChange = (event) => {
        setData(event.target.name, event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        get(route('index', data), {
            preserveScroll: true,
        });
    };

    products.data.forEach((product) => {
        rows.push(
            <ProductCard
                product={product}
                currencyCode={currencyCode}
                key={product.id}
            />
        );
    });

    return (
        <>
            <Head title="Web Store - Main Page"/>
            <div className="min-h-screen flex flex-col">
                <Header/>
                <main className="flex-grow">
                    <div className="flex flex-wrap items-center justify-center max-w-10xl mx-auto p-2 lg:p-4">
                        <form id="searchForm" onSubmit={handleSubmit} className="flex items-center">
                            <div className="flex md:order-2">
                                <div className="relative hidden md:block">
                                    <input
                                        type="number"
                                        id="priceFrom"
                                        name="priceFrom"
                                        value={data.priceFrom}
                                        onChange={handleChange}
                                        className="inline-block m-1 p-1 ps-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Price from..."
                                    />
                                    <input
                                        type="number"
                                        id="priceTo"
                                        name="priceTo"
                                        value={data.priceTo}
                                        onChange={handleChange}
                                        className="inline-block m-1 p-1 ps-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Price to..."
                                    />
                                    <input
                                        type="text"
                                        id="productName"
                                        name="productName"
                                        value={data.productName}
                                        onChange={handleChange}
                                        className="inline-block m-1 p-1 ps-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Search..."
                                    />
                                </div>
                                <button
                                    type="submit"
                                    data-collapse-toggle="navbar-search"
                                    aria-controls="navbar-search"
                                    aria-expanded="false"
                                    className="text-gray-500 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700 focus:outline-none focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 rounded-lg text-sm p-2 ms-1"
                                >
                                    <svg
                                        className="w-5 h-5"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                        />
                                    </svg>
                                    <span className="sr-only">Search</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <Paginator {...products} />
                    <div className="flex flex-wrap items-center justify-center max-w-10xl mx-auto p-2 lg:p-4 gap-4"> {/* Added gap-4 for spacing */}
                        {rows}
                    </div>
                </main>
                <Footer/>
            </div>
        </>
    );
}
