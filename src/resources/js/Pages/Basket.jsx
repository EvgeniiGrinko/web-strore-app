import {Head, useForm, usePage} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

export default function Basket({products, basket, fullSum, fullSumWthDiscount, currencyCode }) {
    const auth  = usePage().props.auth;

    function getImageUrl(name) {
        return new URL(`/${name}`, import.meta.url).href
    }

    const { post } = useForm({});

    const deleteItem = (e) => {
        e.preventDefault();
        const productId = e.target.id.split("-")[0]
        post(route('basket.remove', productId), {
            preserveScroll: true,
        });
    };

    const addItem = (e) => {
        e.preventDefault();
        const productId = e.target.id.split("-")[0]
        post(route('basket.add', productId), {
            preserveScroll: true,
        });
    };

    return (<>
            <Head title="Web Store - Basket"/>
            <Header/>
            <body>
                <div className="relative overflow-x-auto shadow-md sm:rounded-lg m-10">
                    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" className="px-6 py-3 text-center">
                                    Image
                                </th>
                                <th scope="col" className="px-6 py-3 text-center">
                                    Product
                                </th>
                                <th scope="col" className="px-6 py-3 text-center">
                                    Qty
                                </th>
                                <th scope="col" className="px-6 py-3 text-center">
                                    Price
                                </th>
                                <th scope="col" className="px-6 py-3 text-center">
                                    Discount, %
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {Object.keys(basket).map(function (productId, keyName) {
                                return (
                                    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                        <td className="p-4 text-center">
                                            <img src={getImageUrl(`${products[productId].image}`)}
                                                 className="w-16 md:w-32 max-w-full max-h-full"
                                                 alt={`${products[productId].name}`}
                                            />
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white text-center">
                                            {`${products[productId].name}`}
                                        </td>
                                        <td className="px-6 py-4 text-center">
                                            <div className="flex items-center">
                                                <form onSubmit={deleteItem} id={`${productId}` + "-remove"}
                                                      action={route('basket.remove', `${productId}`)} method="post">
                                                    <button
                                                        className="inline-flex items-center justify-center p-1 me-3 text-sm font-medium h-6 w-6 text-gray-500 bg-white border border-gray-300 rounded-full focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                                                        type="submit">
                                                        <svg className="w-3 h-3" aria-hidden="true"
                                                             xmlns="http://www.w3.org/2000/svg"
                                                             fill="none" viewBox="0 0 18 2">
                                                            <path stroke="currentColor" strokeLinecap="round"
                                                                  strokeLinejoin="round"
                                                                  strokeWidth="2" d="M1 1h16"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                                <div>
                                                    <input type="number" id="first_product"
                                                           className="bg-gray-50 w-14 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block px-2.5 py-1 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                           value={`${basket[productId]}`} required disabled
                                                    />
                                                </div>
                                                <form
                                                    onSubmit={addItem}
                                                    id={`${productId}` + "-add"}
                                                    action={route('basket.add', `${productId}`)}
                                                    method="post">
                                                    <button
                                                        className="inline-flex items-center justify-center h-6 w-6 p-1 ms-3 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-full focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                                                        type="submit">
                                                        <svg className="w-3 h-3" aria-hidden="true"
                                                             xmlns="http://www.w3.org/2000/svg"
                                                             fill="none" viewBox="0 0 18 18">
                                                            <path stroke="currentColor" strokeLinecap="round"
                                                                  strokeLinejoin="round"
                                                                  strokeWidth="2" d="M9 1v16M1 9h16"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white text-center">
                                            {parseFloat(`${products[productId].price}`).toFixed(2)}
                                        </td>
                                        <td className="px-6 py-4 text-center">
                                            {`${products[productId].discount_percentage ?? "-"}`}
                                        </td>
                                    </tr>

                                )
                            })}
                            <tr>
                                <td className="px-6 py-3 text-center">Total (without discount):  {parseFloat(`${fullSum}`).toFixed(2)} {currencyCode}</td>
                                <td className="px-6 py-3 text-center"> Total (with discount)</td>
                                <td className="px-6 py-3 text-center">{parseFloat(`${fullSumWthDiscount}`).toFixed(2)} {currencyCode}</td>
                                <td className="px-6 py-3 text-center">To be paid:</td>
                                <td className="px-6 py-3 text-center">{parseFloat(`${fullSumWthDiscount}`).toFixed(2)} {currencyCode}</td>
                            </tr>
                        </tbody>
                    </table>
                    <form id="auth-form" method="POST" className="m-10 max-w-md mx-auto content-end" action={route('order')}>
                        {!auth.user ? (
                            <>
                                <div className="relative z-0 w-full mb-5 group">
                                    <input type="email" name="email" id="email"
                                           className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                           placeholder=" " required/>
                                    <label htmlFor="email"
                                           className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                        Email
                                    </label>
                                </div>
                                <div className="grid md:grid-cols-2 md:gap-6">
                                    <div className="relative z-0 w-full mb-5 group">
                                        <input type="text" name="full-name" id="full-name"
                                               className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                               placeholder=" " required/>
                                        <label htmlFor="full-name"
                                               className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                            Name
                                        </label>
                                    </div>
                                    <div className="relative z-0 w-full mb-5 group">
                                        <input type="tel"
                                               pattern="^\+?[1-9]\d{1,14}$"
                                               name="phone-number"
                                               id="phone-number"
                                               className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                               placeholder=" " required/>
                                        <label htmlFor="phone-number"
                                               className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                                            Phone
                                        </label>
                                    </div>
                                </div>
                            </>
                        ) : (<></>)}
                        <button type="submit"
                                id="buttonCreateOrder"
                                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Create
                            Order
                        </button>
                    </form>
                </div>
            </body>
        <Footer/>
    </>);
}
