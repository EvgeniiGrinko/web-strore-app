import {Head} from '@inertiajs/react';
import Header from "@/Pages/Components/Header.jsx";
import Footer from "@/Pages/Components/Footer.jsx";

export default function UserPage({user}) {

    let title = "Web Store - User " + user.name;
    let orders = user.orders;

    return (<>
        <Head title={title}/>
        <Header/>
        <body>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg m-10">
                <h1>
                    Customer System Number № {`${user.id}`}
                </h1>
                <p>
                    Customer: <b>{`${user.name}`}</b>
                </p>
                <p>
                    Telephone Number: <b>{`${user.phone_number}`}</b>
                </p>
                <p>
                    List of Orders
                </p>
                <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Order Number
                        </th>
                        <th scope="col" className="px-6 py-3 content-center">
                            Order Creation Date
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Sum
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(orders).map(function (orderId, keyName) {
                        return (<>
                            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <td className="p-4">
                                    {`${orderId}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].name}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    {`${orders[orderId].merchant_trans_amount}`} {`${orders[orderId].merchant_currency}`}
                                </td>
                                <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                                    <div className="btn-group" role="group">
                                        <a className="btn btn-success" type="button"
                                           href={route('orders.show', `${order.id}`)}
                                        >Open</a>
                                    </div>
                                </td>
                            </tr>
                        </>)
                    })}
                    </tbody>
                </table>
            </div>
        </body>
        <Footer/>
    </>);
}
