function sendReqForCheck() {
    const agr_trans_id = document.getElementById("agr_trans_id").value;
    $.ajax(
        {
            type: "POST",
            async: true,
            url: "/check",
            data: {agr_trans_id: agr_trans_id},
            dataType: 'json',
        }
    ).done(function (msg) {
        document.getElementById("status").innerHTML = msg['status'];
    });
}
function sendReqForPayout() {
    const payout_sum = document.getElementById("payout_sum").value;
    // const token = document.getElementById("_token").value;
    const token = document.querySelector('meta[name="csrf-token"]').content;
    if (!payout_sum) {
        return;
    }
    document.getElementById("payout-link-group").style.display = 'none';
    document.getElementById("payout_link").innerHTML = '';
    document.getElementById("payout_link").href = '';
    document.getElementById("errorPayoutLink").style.display = 'none';
    document.getElementById("errorPayoutLink").innerHTML = '';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax(
        {
            type: "POST",
            async: true,
            url: "/payout",
            data: {payout_sum: payout_sum, token: token},
            dataType: 'json',
        }
    ).done(function (msg) {
        if (msg.pg_redirect_url)
        {
            document.getElementById("payout-link-group").style.display = '';
            document.getElementById("payout_link").innerHTML = msg.pg_redirect_url;
            document.getElementById("payout_link").href = msg.pg_redirect_url;
        } else if (msg.error)
        {
            console.log(msg.error);
            document.getElementById("errorPayoutLink").style.display = '';
            document.getElementById("errorPayoutLink").innerHTML = msg.error;
        }

    });
}
function sendReqForCheckOrderPaymentState(id) {

    $.ajax(
        {
            type: "POST",
            async: true,
            url: "/orders-check",
            data: {order_id: id},
            dataType: 'json',
        }
    ).done(function (msg) {
        document.getElementById("order-status").innerHTML = msg['status'];
    });
}

function sendReqForCreatingOrder(authStatus) {

    data = {};
    if(authStatus === 0){
        var fullName = document.getElementById("full-name").value;
        var phoneNumber = document.getElementById("phone-number").value;
        var email = document.getElementById("email").value;
        data = {
            full_name: fullName,
            phone_number: phoneNumber,
            email
        }
    }
    $.ajax(
        {
            type: "POST",
            async: true,
            url: "/order",
            data: data,
            dataType: 'json',
        }
    ).done(function (msg) {

        document.getElementById('buttonCreateOrder').setAttribute('disabled', '');
        // document.getElementById('clearBasket').setAttribute('disabled', '');
        // document.getElementById('buttonPay').setAttribute('style', 'display: flex');
        const nodeList = document.querySelectorAll("form > button");
        for (let i = 0; i < nodeList.length; i++) {
            nodeList[i].setAttribute('disabled', '');
        }
        window.location = window.location.origin + '/payment';



    });
}

function sendReqForPayment(orderId) {
    var payment_type = document.getElementById('payment-type').value;
            $.ajax(
                {
                    type: "POST",
                    async: true,
                    url: "/pay",
                    data: {
                        order_id : orderId,
                        payment_type: payment_type,
                    },
                    dataType: 'json',
                }
            ).done(function (msg) {
                if(msg.url && (msg.status === 1 || msg.status === 0)){
                    document.getElementById("iframe").setAttribute("src", msg.url);
                    document.getElementById("popup").style.display = 'flex';


                } else if(msg.status === 2){
                    alert('Этот платеж успешно проведен')

                } else if(msg.status === 3){
                    alert('Этот платеж отменен ранее')
                }
            });
}

function closeButton(orderId){
   // if(confirm("Do you want to close this popup?\nEither OK or Cancel.")){
     document.getElementById("popup").style.display = 'none';
    var statusRecieved = null;
    var intervalId = setTimeout(function(){

        $.ajax(
            {
                type: "POST",
                async: true,
                url: "/merchant-transaction-check",
                data: {
                    order_id : orderId,
                    check: 'After payment'
                },
                dataType: 'json',
            }
        ).done(function (msg) {
             document.getElementById('payment-state').innerText = msg.payment_state === 2 ? 'Оплачен': msg.payment_state === 3 ?'Отменен' : 'Не оплачен';
             document.getElementById('payment-type').setAttribute('disabled', '');
             statusRecieved = msg.transaction_type;
        })

    }, 2000);
}
const copyToClipboard = str => {
    if (navigator && navigator.clipboard && navigator.clipboard.writeText)
        return navigator.clipboard.writeText(str);
    //return Promise.reject('The Clipboard API is not available.');
};
document.body.addEventListener('pointerdown', function (event){
    if (document.getElementById("payout_link")) {
        const copyText = document.getElementById("payout_link").innerText;

        if (event.target.id === 'payout_link') {
            copyToClipboard(copyText);
        }
        copyToClipboard(copyText);
    }
} );

function getLink() {
    // Get the text field
    const copyText = document.getElementById("payout_link");

    const myEvent = new PointerEvent('pointerdown')

    copyText.dispatchEvent(myEvent);

}

document.addEventListener('DOMContentLoaded',function () {
    if (navigator && navigator.clipboard && navigator.clipboard.writeText && document.getElementById('copy_container')) {
        document.getElementById('copy_container').innerHTML = '<img id="copyId2"  onclick="getLink()" height=\'15px\' src="/images/copy-icon.svg">\n'
    }
});




