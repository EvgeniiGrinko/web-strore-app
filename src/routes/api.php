<?php

declare(strict_types=1);

use App\Http\Controllers\Api\ApiPayController;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Controllers\APIProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/check', [ApiPayController::class, 'check'])->name('check_freedom');
Route::post('/notify', [ApiPayController::class, 'notify'])->name('result_freedom');

//Route::post('/products', 'App\Http\Controllers\APIProductController@index')->middleware('requestHasAuthKey');;
Route::apiResource('/product', ProductsController::class);
