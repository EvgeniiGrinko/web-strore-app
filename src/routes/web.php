<?php

declare(strict_types=1);

use App\Http\Controllers\BasketController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PayController;
use App\Http\Controllers\PayoutController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/', [MainController::class, 'index'])->name('index');

Route::post('/pay/{order}', [PayController::class, 'pay'])->name('pay');
Route::get('/payment', [PayController::class, 'showPaymentPage'])->name('show.payment.page');
Route::post('/order', [OrderController::class, 'createOrder'])->name('order');

Route::get('/basket', [BasketController::class, 'index'])->name('basket');
Route::post('/basket/add/{product}', [BasketController::class, 'addItem'])->name('basket.add');
Route::post('/basket/remove/{product}', [BasketController::class, 'deleteItem'])->name('basket.remove');
Route::get('/basket/clear', [BasketController::class, 'clear'])->name('basket.clear');

Route::get('/seed', [MainController::class, 'seedDB'])->name('seed');
Route::get('/product/{product}', [MainController::class, 'product'])->name('product.show');
Route::post('/merchant-transaction-check', [MainController::class, 'checkMerchantTransaction']);
Route::get('/privacy-policy', [MainController::class, 'privacyPolicy'])->name('privacy.policy');

Route::middleware(['auth'])->group(function () {
    Route::get('/payout', [PayoutController::class, 'payOutView'])->name('payout');
    Route::post('/payout', [PayoutController::class, 'payOutInit'])->name('payout.init');
    Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');
    Route::get('/orders/{order}', [OrderController::class, 'order'])->name('orders.show');
    Route::post('/orders-check', [OrderController::class, 'checkPaymentState'])->name('order.check');
    Route::get('/user/{user}', [UserController::class, 'index'])->name('user');
});

require __DIR__.'/auth.php';
