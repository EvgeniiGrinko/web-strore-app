<?php

declare(strict_types=1);

namespace App\PaymentGateways;

use App\Enums\PaymentDriversEnum;
use App\Models\PaymentDriver;
use App\PaymentGateways\FreedomPay\FreedomPay;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Manager;
use Override;

class PaymentGatewayManager extends Manager
{

    protected string $defaultDriver;

    public function __construct(Container $container)
    {
        $this->setActiveDriver();
        parent::__construct($container);
    }

    public function createFreedomPayDriver(): FreedomPay
	{
        return $this->container->make(FreedomPay::class);
	}

    #[Override]
    public function getDefaultDriver(): string
    {
        return $this->defaultDriver;
    }

    private function setActiveDriver(): void
    {
        $driver = PaymentDriver::query()->where('is_active', 1)->first();

        $driver ? $this->defaultDriver =
            PaymentDriversEnum::getClass($driver->code) : $this->config->get('payment_drivers.default');

    }
}
