<?php

declare(strict_types=1);

namespace App\PaymentGateways\Facades;

use App\PaymentGateways\BaseDriver\Interfaces\PaymentDriverContract;
use App\PaymentGateways\PaymentGatewayManager;
use Illuminate\Support\Facades\Facade;

class Payment extends Facade
{
    /**
     * @method static PaymentDriverContract driver(string $key)
     * @see PaymentGatewayManager
     */
    protected static function getFacadeAccessor(): string
    {
        return PaymentGatewayManager::class;
    }

}
