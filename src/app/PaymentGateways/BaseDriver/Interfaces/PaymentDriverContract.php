<?php

declare(strict_types=1);

namespace App\PaymentGateways\BaseDriver\Interfaces;

use App\Models\Order;

interface PaymentDriverContract
{

    public function pay(Order $order);

    public function refund(Order $order);

    public function status(Order $order);

}
