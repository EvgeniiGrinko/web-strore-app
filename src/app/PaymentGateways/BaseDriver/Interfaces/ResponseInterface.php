<?php

declare(strict_types=1);

namespace App\PaymentGateways\BaseDriver\Interfaces;

interface ResponseInterface
{
    public function isOperationSuccess(): bool;

    public function isOperationFail(): bool;

}
