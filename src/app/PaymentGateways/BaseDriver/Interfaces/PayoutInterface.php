<?php

declare(strict_types=1);

namespace App\PaymentGateways\BaseDriver\Interfaces;

interface PayoutInterface
{
    public function handle();
}
