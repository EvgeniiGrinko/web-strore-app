<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay;

use Illuminate\Support\Facades\Log;
use Throwable;

trait LogTrait
{
    public const string MESSAGE_ADDON = 'FreedomPay Driver: ';

    public function logException(Throwable $t): void
    {
        Log::error(self::MESSAGE_ADDON . 'Exception occurred', [
            'code'    => $t->getCode(),
            'message' => $t->getMessage(),
            'file'    => $t->getFile(),
            'line'    => $t->getLine(),
            'trace'   => $t->getTrace(),
        ]);
    }

    public function logInfoMessage(string $message, array $context = []): void
    {
        Log::info(self::MESSAGE_ADDON . $message, $context);
    }
}
