<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay;

use App\Models\MerchantTransaction;
use App\Models\Order;
use App\PaymentGateways\BaseDriver\Interfaces\PaymentDriverContract;
use App\PaymentGateways\FreedomPay\Actions\Pay\Service as PayService;
use App\PaymentGateways\FreedomPay\Actions\PayOut\Service as PayOutService;
use App\PaymentGateways\FreedomPay\Actions\Refund\Service as RefundService;
use App\PaymentGateways\FreedomPay\Actions\Status\Service as StatusService;

class FreedomPay implements PaymentDriverContract
{

    public function pay(Order $order): MerchantTransaction
    {
        return (new PayService())->handle($order);
    }

    public function payOut(string $payOutSum)
    {
        return (new PayOutService())->handle($payOutSum);
    }

    public function status(Order $order)
    {
        return (new StatusService())->handle($order);
    }

    public function refund(Order $order)
    {
        return (new RefundService())->handle($order);
    }
}
