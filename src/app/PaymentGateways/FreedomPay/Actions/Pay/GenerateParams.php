<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Pay;

use App\Models\Order;
use App\PaymentGateways\FreedomPay\Helper;

class GenerateParams
{
    /** @return array<mixed> */
    public static function createParams(Order $order): array
    {
        $pg_merchant_id = config('secret_key.freedom.freedompay_merchant_id');

        $request = $requestForSignature = [
            'pg_order_id'    => $order->id,
            'pg_merchant_id' => $pg_merchant_id,
            'pg_amount'      => $order->merchant_trans_amount,
            'pg_description' => 'Order number ' . $order->id,
            'pg_salt'        => 'Here might be your advertisement',
            'pg_currency'    => 'KGS',
        ];

        $requestForSignature = Helper::makeFlatParamsArray($requestForSignature);
        $request['pg_sig']   = self::makeSignature($requestForSignature);

        return $request;
    }

    public static function makeSignature(array $requestForSignature): string
    {
        $secret_key = config('secret_key.freedom.freedompay_merchant_secret');

        ksort($requestForSignature);
        array_unshift($requestForSignature, 'init_payment.php');
        $requestForSignature[] = $secret_key;

        return md5(implode(';', $requestForSignature));
    }
}
