<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Pay;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class Request
{
    /** @var array<string>> */
    public array $headers;

    /** @param  array<mixed> $params */
    public function __construct(public array $params)
    {
        $this->headers = [
            'Content-type' => 'form-data',
        ];
    }

    /**
     * @throws GuzzleException
     */
    public function send(): ResponseInterface
    {
        Log::channel('freedomCreatePaymentRequest')->info(
            'Create payment request for order id ' . $this->params["pg_order_id"] . ' : ',
            [
                'headers' => $this->headers,
                'params'  => $this->params,
            ]
        );

        $client = new Client();

        return $client->request('GET', config('urls.freedom.init') . '?' . http_build_query($this->params), [
            RequestOptions::HEADERS         => $this->headers,
            RequestOptions::TIMEOUT         => 10.0,
            RequestOptions::CONNECT_TIMEOUT => 10.0,
        ]);
    }
}
