<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Pay;

use App\Enums\MerchantTransactionStatusEnum;
use App\Enums\StoreExceptionsEnum;
use App\Exceptions\StoreException;
use App\Models\MerchantTransaction;
use App\Models\Order;
use App\PaymentGateways\FreedomPay\LogTrait;
use App\Repositories\MerchantTransactionRepository;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Log;

class Service
{
    use LogTrait;

    public int $countOfFailures;
    public int $limitOfFailures;
    public array $params;

    public function __construct()
    {
        $this->countOfFailures = 0;
        $this->limitOfFailures = 15;
    }

    public function handle(Order $order): MerchantTransaction
    {
        try {
            $this->params = GenerateParams::createParams($order);
            $createPaymentRequest = new Request($this->params);
            $newMerchantTransactionRepository = new MerchantTransactionRepository();

            $merchantTransaction = $newMerchantTransactionRepository->createMerchantTransaction($this->params, $order);

            $merchantTransaction->setRequestData(json_encode([
                'headers' => $createPaymentRequest->headers,
                'json' => $createPaymentRequest->params,
            ]));
            $merchantTransaction->save();
            $response = $createPaymentRequest->send();

            $contents = $response->getBody()->getContents();
            $resp = simplexml_load_string($contents);
            Log::channel('freedomCreatePaymentRequest')->info('Create payment response for order id ' . $order->id
                . ' response: ', [$contents]);

            if ($response->getStatusCode() === 200) {
                $merchantTransaction->response_data = $contents;
                $merchantTransaction->pg_status = $resp->pg_status ?? null;
                $merchantTransaction->pg_payment_id = $resp->pg_payment_id ?? null;
                $merchantTransaction->pg_redirect_url = $resp->pg_redirect_url ?? null;
                if ($resp->pg_status == 'success') {
                    $merchantTransaction->state = MerchantTransactionStatusEnum::SUCCESS->code();
                } elseif ($resp->pg_status == 'error') {
                    $merchantTransaction->state = MerchantTransactionStatusEnum::ERROR->code();
                } else {
                    $merchantTransaction->state = MerchantTransactionStatusEnum::HOLD->code();
                }

                $merchantTransaction->save();
            }
        } catch (Exception $exception) {

            $this->logException($exception);

            $merchantTransaction->response_data = json_encode([
                "Exception" => [
                    "code" => $exception->getCode(),
                    "message" => $exception->getMessage(),
                ],
            ]);
            $merchantTransaction->state = -1; // статус -1 - неуспешно, ошибка
            $merchantTransaction->save();
            if (is_a($exception, BadResponseException::class)) {
                if ($exception->getCode() === 429 || $exception->getCode() >= 500) {
                    $this->countOfFailures++;
                    if ($this->countOfFailures < $this->limitOfFailures) {
                        return $this->handle($order);
                    }
                    throw new StoreException(StoreExceptionsEnum::SYSTEM_ERROR_MESSAGE);
                }
            }
        }
        return $merchantTransaction;
    }
}

