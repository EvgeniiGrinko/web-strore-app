<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\PayOut;

use App\Enums\StoreExceptionsEnum;
use App\Exceptions\StoreException;
use App\PaymentGateways\FreedomPay\LogTrait;
use Exception;

class Service
{
    use LogTrait;

    public int $countOfFailures;
    public int $limitOfFailures;

    public function __construct()
    {
        $this->countOfFailures = 0;
        $this->limitOfFailures = 15;
    }

    public function handle(string $payOutSum): Response
    {
        $resp = false;
        try {
            $params = GenerateParams::createParams((float)$payOutSum);
            $createPaymentRequest = new Request($params);
            $guzzleResponse = match ((bool)env('APP_DEBUG', false)) {
                true => $createPaymentRequest->send(),
                false => $createPaymentRequest->sendDev()
            };

            $contents = $guzzleResponse->getBody()->getContents();
            $response = new Response(simplexml_load_string($contents));

            $this->logInfoMessage(
                'Create payout response for order id ' . $params['pg_order_id'] . ' response: ',
                [
                    $contents
                ]
            );

            if ($guzzleResponse->getStatusCode() === 200) {
                if ($response->status == 'success') {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Success->code();
                } elseif ($response->status == 'error') {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Error->code();
                } else {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Hold->code();
                }

            }
            return $response;
        } catch (Exception $exception) {
            $this->logException($exception);

            throw new StoreException(StoreExceptionsEnum::SYSTEM_ERROR_MESSAGE);

        }
    }
}
