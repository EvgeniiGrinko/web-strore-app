<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Refund;

use Illuminate\Support\Str;

class GenerateParams
{
    public static function createParams(float $payOutSum): array
    {
        $pg_merchant_id = 542610;
        $secret_key = 'hzJO3HuDAHC7KBM1';
        $orderId = rand(123490, 4567899) . Str::uuid()->toString();
        $deadline = date('Y-m-d H:i:s', time() + 86400);

        $request = [
            'pg_merchant_id' => $pg_merchant_id,
            'pg_amount' => $payOutSum,
            'pg_order_id' => $orderId,
            'pg_description' => 'Выплата # ' . $orderId . ' на сумму ' . $payOutSum,
            'pg_post_link' => route('index'),
            'pg_back_link' => route('index'),
            'pg_order_time_limit' => $deadline,
            'pg_salt' => 'That All U Need To Know',
        ];

        //generate a signature and add it to the array
        ksort($request); //sort alphabetically
        array_unshift($request, 'reg2nonreg');
        array_push($request, $secret_key);
        $request['pg_sig'] = md5(implode(';', $request)); // signature
        unset($request[0], $request[1]);
        return  $request;
    }
}
