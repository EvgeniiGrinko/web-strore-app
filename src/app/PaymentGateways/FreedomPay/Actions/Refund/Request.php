<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Refund;

use App\PaymentGateways\FreedomPay\LogTrait;
use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Request
{
    use LogTrait;

    /** @var array<string> */
    public array $headers;

    /** @param array<mixed> $params */
    public function __construct(public array $params)
    {
        $this->headers = [
            'Content-type' => 'form-data',
        ];
    }

    /**
     * @throws GuzzleException
     */
    public function send(): ResponseInterface
    {
        $this->logInfoMessage(
            'Create payout payment request for order id ' . $this->params["pg_order_id"],
            [
                'headers' => $this->headers,
                'params' => $this->params,
            ]);

        $client = new Client();

        return $client->request(
            'POST',
            config('urls.freedom.payout') . '?' . http_build_query($this->params),
            [
                RequestOptions::HEADERS => $this->headers,
            ]);
    }

    /**
     * @throws Exception
     */
    public function sendDev(): ResponseInterface
    {
        $this->logInfoMessage(
            'Dev write-off transaction request for driver id ' . $this->params["driver_profile_id"],
            [
                'headers' => $this->headers,
                'params' => $this->params,
            ]);

        //throw new \GuzzleHttp\Exception\ClientException('Текстовое описание ошибки', new \GuzzleHttp\Psr7\Request('POST', 'url'), new \GuzzleHttp\Psr7\Response(410));
        //throw new TransferException('Текстовое описание ошибки', 2);

        $datetime = new DateTimeImmutable("now");

        $body = json_encode([
            "amount" => $this->params["amount"],
            "category_id" => $this->params["category_id"],
            "created_by" => [
                "dispatcher_id" => "3621b3429ea04d989adf5ff4d5515d6a",
                "dispatcher_name" => "Вася Пупкин",
                "identity" => "dispatcher",
                "passport_uid" => "123456789",
            ],
            "currency_code" => "KGS",
            "description" => "Списание №100",
            "driver_profile_id" => $this->params["driver_profile_id"],
            "event_at" => $datetime->format('c'),
            "park_id" => $this->params["park_id"],
        ]);
        if ($body === false) {
            throw new Exception("Body of the Response can't be false");
        }
        return new Response(
            200,
            ['Content-Type' => 'application/json'],
            $body
        );
    }
}
