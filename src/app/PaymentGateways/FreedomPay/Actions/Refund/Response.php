<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Refund;

use App\PaymentGateways\FreedomPay\Base\BaseResponse;

class Response extends BaseResponse
{

}
