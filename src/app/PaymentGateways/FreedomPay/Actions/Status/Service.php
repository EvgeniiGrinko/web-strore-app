<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Actions\Status;

use App\Enums\StoreExceptionsEnum;
use App\Exceptions\StoreException;
use App\Models\Order;
use App\PaymentGateways\FreedomPay\LogTrait;
use Exception;
use Illuminate\Support\Facades\Log;

class Service
{
    use LogTrait;

    public int $countOfFailures;
    public int $limitOfFailures;

    public function __construct()
    {
        $this->countOfFailures = 0;
        $this->limitOfFailures = 15;
    }

    public function handle(Order $order): Response
    {
        $resp = false;
        try {
            $params = GenerateParams::createParams((float)$payOutSum);
            $createPaymentRequest = new CreatePaymentRequest($params);
            //            $newMerchantTransactionRepository = new MerchantTransactionRepository();
            //
            //            //$merchantTransaction = $newMerchantTransactionRepository->createMerchantTransaction($params, $order);
            //
            //            $merchantTransaction->request_data = json_encode([
            //                'headers' => $createPaymentRequest->headers,
            //                'json' => $createPaymentRequest->params
            //            ]);
            //            $merchantTransaction->save();

            $guzzleResponse = match ((bool)env('APP_DEBUG', false)) {
                true => $createPaymentRequest->send(),
                false => $createPaymentRequest->send_dev()
            };

            $contents = $guzzleResponse->getBody()->getContents();
            //dd(simplexml_load_string($contents));
            $response = new Response(simplexml_load_string($contents));;
            Log::channel('freedomCreatePayOutRequest')->info('Create payout response for order id ' . $params['pg_order_id']
                . ' response: ', [$contents]);

            if ($guzzleResponse->getStatusCode() === 200) {
                //                $merchantTransaction->response_data =  $contents ?? null;
                //                $merchantTransaction->pg_status = $resp->pg_status ?? null;
                //                $merchantTransaction->pg_payment_id = $resp->pg_payment_id ?? null;
                //                $merchantTransaction->pg_redirect_url = $resp->pg_redirect_url ?? null;
                if ($response->status == 'success') {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Success->code();
                } elseif ($response->status == 'error') {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Error->code();
                } else {
                    //                    $merchantTransaction->state = MerchantTransactionStatusEnum::Hold->code();
                }

            }
            return $response;
        } catch (Exception $exception) {
            $this->logException($exception);

            throw new StoreException(StoreExceptionsEnum::SYSTEM_ERROR_MESSAGE);

        }
    }
}
