<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay\Base;

use App\PaymentGateways\BaseDriver\Interfaces\ResponseInterface;
use Exception;
use SimpleXMLElement;

class BaseResponse implements ResponseInterface
{
    public string $status;

    /**
     * @throws Exception
     */
    public function __construct(private readonly SimpleXMLElement|false $response)
    {
        $this->create();
    }

    /**
     * @throws Exception
     */
    public function create(): void
    {
        if (is_bool($this->response)) {
            throw new Exception('Error getting occurred while requesting Payout');
        }
        /** @var string $status */
        $status = $this->response['status'];
        $this->status = $status;
    }

    #[\Override] public function isOperationSuccess(): bool
    {
        // TODO: Implement isOperationSuccess() method.
    }

    #[\Override] public function isOperationFail(): bool
    {
        // TODO: Implement isOperationFail() method.
    }
}
