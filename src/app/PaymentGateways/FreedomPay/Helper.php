<?php

declare(strict_types=1);

namespace App\PaymentGateways\FreedomPay;

class Helper
{
    /**
     * @param array<mixed> $arrParams
     * @return  array<mixed>
     */
    public static function makeFlatParamsArray(array $arrParams, string $parent_name = ''): array
    {
        $arrFlatParams = [];
        $i = 0;

        foreach ($arrParams as $key => $val) {
            $i++;
            /**
             * Имя делаем вида tag001subtag001
             * Чтобы можно было потом нормально отсортировать и вложенные узлы не запутались при сортировке
             */
            $name = $parent_name . $key . sprintf('%03d', $i);
            if (is_array($val)) {
                $arrFlatParams = array_merge($arrFlatParams, self::makeFlatParamsArray($val, $name));
                continue;
            }
            $arrFlatParams += [$name => (string)$val];
        }

        return $arrFlatParams;
    }
}
