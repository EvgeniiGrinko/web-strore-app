<?php

declare(strict_types=1);

namespace App\PaymentGateways;

use App\Exceptions\PaymentSystemDriverException;
use App\Http\Controllers\Controller;
use App\Models\PaymentDriver;
use Illuminate\Http\Request;

class PaymentGatewaySwitcher extends Controller
{
    protected $paymentDriver;
    protected $gatewayManager;

    public function __construct(PaymentGatewayManager $gatewayManager)
    {
        $this->gatewayManager = $gatewayManager;
    }

    public function request(Request $request, string $paymentDriverCode, int $merchantId): mixed
    {
        $ps = PaymentDriver::byCode($paymentDriverCode)->first();

        if ($ps) {
            $driver = $this->gatewayManager->driver($ps->getDriver()->getClassCode());
            try {
                return $driver->request($request, $ps, $merchantId);
            } catch (PaymentSystemDriverException $e){
                return response($e->getMessage(), 200)->header('Content-Type', 'application/xml');
            }
        }

        abort(404);
    }
}
