<?php

declare(strict_types=1);

namespace App\PaymentGateways;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
    }

    public function register()
    {
        $this->app->singleton(PaymentGatewayManager::class, function($app) {
            return new PaymentGatewayManager($app);
        });
    }

}
