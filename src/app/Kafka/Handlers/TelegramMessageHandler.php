<?php

declare(strict_types=1);

namespace App\Kafka\Handlers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Junges\Kafka\Contracts\KafkaConsumerMessage;

class TelegramMessageHandler
{
    /**
     * @throws GuzzleException
     */
    public function __invoke(KafkaConsumerMessage $message)
    {
        $client         = new Client();
        $messageBody    = $message->getBody();
        $messageHeaders = $message->getHeaders();

        return $client->request(
            $messageBody['method'],
            $messageBody['url'],
            [
                $messageHeaders['format'] => [
                    'chat_id' => $messageBody['chat_id'],
                    'text'    => $messageBody['text'],
                ],
            ]
        );
    }
}
