<?php

declare(strict_types=1);

namespace App\Facades;

use App\Models\Order;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;
use Psr\Http\Message\ResponseInterface;

class Telegram
{
    private string $chatIdToReport;
    private string $botToken;
    private string $apiEndpoint;

    public function __construct()
    {
        /**
         * @param string $chatIdToReport
         */
        $chatIdToReport       = config('telegram.report_chat_id');
        $this->chatIdToReport = $chatIdToReport;

        /**
         * @param string $botToken
         */
        $botToken       = config('telegram.telegram_bot_token');
        $this->botToken = $botToken;

        /**
         * @param string $apiEndpoint
         */
        $apiEndpoint       = config('telegram.api_endpoint');
        $this->apiEndpoint = $apiEndpoint;
    }

    public function sendMessage(array $data): ResponseInterface
    {
        try {
            $producer = Kafka::publishOn('telegram_message');
            $message  = new Message(
                headers: ['format' => 'json'],
                body: [
                    'method'  => 'POST',
                    'url'     => $this->apiEndpoint . $this->botToken . '/sendMessage',
                    'chat_id' => $this->chatIdToReport,
                    'text'    => $this->prepareMessage($data),
                ],
                key: 'telegram_message'
            );
            $producer->withConfigOptions([
                'debug' => 'all',
            ])
                ->withDebugEnabled()
                ->withMessage($message);

            $producer->send();
        } catch (\Throwable $t) {
            Log::info('Exception sending message to Kafka occurred', [
                'code'    => $t->getCode(),
                'message' => $t->getMessage(),
                'line'    => $t->getLine(),
                'file'    => $t->getFile(),
                'trace'   => $t->getTraceAsString(),
            ]);
        }
        $client = new Client();
        return $client->request(
            'POST',
            $this->apiEndpoint . $this->botToken . '/sendMessage',
            [
                'json' => [
                    'chat_id' => $this->chatIdToReport,
                    'text'    => $this->prepareMessage($data),
                ],
            ]
        );
    }

    public function reportOrderCreated(Order $order): ResponseInterface
    {
        $goods = [];
        $data = [
            'orderId'               => $order->id,
            'goods'                 => $goods,
            'customer_fname'        => $order->customer_fname,
            'customer_email'        => $order->customer_email,
            'customer_phone_number' => $order->phone_number,
            'merchant_trans_amount' => $order->merchant_trans_amount,
            'merchant_currency'     => $order->merchant_currency,
            'state'                 => $order->state,
            'payment_state'         => $order->state,
            'vendorId'              => $order->vendor_id,
            'vendorName'            => $order->merchant->name,
        ];

        return $this->sendMessage($data);
    }

    public function prepareMessage(array $data): string
    {
        return 'Создан заказ №' . $data['orderId']  . ' на сумму ' . $data['merchant_trans_amount'] . ' '
            . $data['merchant_currency']
            . '  Данные пользователя: ' . $data['customer_fname'] .
            ', телефон ' . $data['customer_phone_number'] . ', email '
            . $data['customer_email'] . ',  Данные магазина: ' . $data['vendorName'] . ' ID ' . $data['vendorId'];
    }
}
