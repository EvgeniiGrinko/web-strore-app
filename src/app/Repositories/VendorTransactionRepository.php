<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\VendorTransactionRepositoryInterface;
use App\Models\VendorTransaction;

class VendorTransactionRepository implements VendorTransactionRepositoryInterface
{
    public function createVendorTransaction(array $vendorPaymentDetails): int
    {
        $data = [];

        $data['vendor_id']             = $vendorPaymentDetails['VENDOR_ID'];
        $data['payment_id']            = $vendorPaymentDetails['PAYMENT_ID'];
        $data['payment_name']          = $vendorPaymentDetails['PAYMENT_NAME'];
        $data['agr_trans_id']          = $vendorPaymentDetails['AGR_TRANS_ID'];
        $data['merchant_trans_id']     = $vendorPaymentDetails['MERCHANT_TRANS_ID'];
        $data['merchant_trans_amount'] = $vendorPaymentDetails['MERCHANT_TRANS_AMOUNT'];
        $data['environment']           = $vendorPaymentDetails['ENVIRONMENT'];
        $data['merchant_trans_data']   = $vendorPaymentDetails['MERCHANT_TRANS_DATA'] ?? null;
        $data['sign_time']             = date("Y-m-d\TH:i:s", $vendorPaymentDetails['SIGN_TIME'] / 1000);
        $data['sign_string']           = $vendorPaymentDetails['SIGN_STRING'];
        $data['state']                 = 1;
        $data['vendor_trans_id']       = VendorTransaction::generateVendorTransactionId();

        $vendorTransaction = VendorTransaction::create($data);
        $vendorTransaction->paymentSystems()->attach($vendorPaymentDetails['PAYMENT_ID']);

        return $data['vendor_trans_id'];
    }
}
