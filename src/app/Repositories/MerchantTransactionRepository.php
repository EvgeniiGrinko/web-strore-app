<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\MerchantTransactionRepositoryInterface;
use App\Models\MerchantTransaction;
use App\Models\Order;

class MerchantTransactionRepository implements MerchantTransactionRepositoryInterface
{
    public function createMerchantTransaction(array $params, Order $order): MerchantTransaction
    {
        return MerchantTransaction::firstOrCreate(
            [
                    'merchant_trans_id' => $params['pg_order_id'],
                ],
            [
                    'vendor_id'             => $params['pg_merchant_id'],
                    'merchant_trans_amount' => $params['pg_amount'],
                    'merchant_currency'     => $params['pg_currency'],
                    'merchant_trans_note'   => $params['pg_description'],
                    'merchant_trans_data'   => $params['pg_description'],
                    'sign_time'             => date("Y-m-d\TH:i:s", time()),
                    'state'                 => 1,
                    'type_transaction_code' => 0,
                    'order_id'              => $order->id,
                    'sign_string'           => $params['pg_sig'],
            ]
        );
    }
}
