<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\Merchant;
use App\Models\MerchantTransaction;
use App\Models\Order;
use App\Models\Product;
use App\Models\VendorTransaction;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @param Merchant $merchant
     * @param array $params_from_request
     * @return Order
     * @throws BindingResolutionException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @param Collection<Product> $products
     */
    public function createOrder(Merchant $merchant, Collection $products, array $params_from_request): Order
    {
        $params_for_creating_order = [
            'vendor_id'             => $merchant->vendor_id,
            'merchant_trans_id'     => MerchantTransaction::generateTransactionId(),
            'merchant_trans_amount' => 0,
            'merchant_currency'     => $merchant->currency->first()->en_name,
            'customer_email'        => $params_from_request['email'] ?? Auth::user()->email,
            'customer_fname'        => $params_from_request['full-name'] ?? Auth::user()->name,
            'phone_number'          => $params_from_request['phone-number'] ?? Auth::user()->phone_number,
            'user_id'               => Auth::check() ? Auth::user()->id : null,
            'state'                 => 1,
        ];

        $order = Order::create($params_for_creating_order);

        foreach ($products as $product) {
            $count = session()->get('basket')['products'][$product->id];

            $order->products()->attach(
                $product->id,
                [
                    'count'          => $count,
                    'price'          => $product->price,
                    'discount_price' => isset($product->discount) ? floor((($product->price / 100) * (100 - $product->discount->percentage))) : null,
                    'discount'       =>  isset($product->discount) ? $product->discount->percentage : null,
                    "discount_sum"   => isset($product->discount) ? (($product->price * $count) - ($count * (floor((($product->price / 100) * (100 - $product->discount->percentage)))))) : null,
                ]
            );

            $product->count -= $count;
            $product->save();
        }

        $order->merchant_trans_amount = $order->getFullSumWithDiscount();
        session(['orderId' => $order->id]);
        $order->update();

        return $order;
    }

    public function findOrder(string $merchant_trans_id)
    {
        try {
            return Order::where('merchant_trans_id', $merchant_trans_id)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return false;
        }
    }
    public function findOrderByVendorTransactionId(string $vendor_trans_id)
    {
        try {
            $vendorTransaction = VendorTransaction::where('vendor_trans_id', $vendor_trans_id)->firstOrFail();
            return $vendorTransaction->merchantTransaction->order;
        } catch (ModelNotFoundException $exception) {
            return false;
        }
    }
}
