<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class PaymentSystemDriverException extends Exception
{
}
