<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Enums\StoreExceptionsEnum;
use Exception;
use Illuminate\Support\Facades\Log;

class StoreException extends Exception
{
    public function __construct(private readonly StoreExceptionsEnum $case, \Throwable $throwable = null)
    {
        parent::__construct(
            $case->value,
            $case->code(),
            $throwable
        );

        Log::error(
            'Exception occurred',
            [
                "case->value" => $case->value,
                "code" => $case->code(),
                "throwable" => $throwable,
                "trace" => $this->getTrace(),
            ]
        );
    }

    public function getEnum(): StoreExceptionsEnum
    {
        return $this->case;
    }
}
