<?php

declare(strict_types=1);

namespace App\Http\Actions;

use App\Models\Merchant;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class IndexAction
{
    public function handle(Request $request): Response
    {
        $productsQuery = Product::query();

        if ($request->filled('priceFrom')) {
            $productsQuery->where('price', '>=', $request->priceFrom);
        }

        if ($request->filled('priceTo')) {
            $productsQuery->where('price', '<=', $request->priceTo);
        }

        if ($request->filled('productName')) {
            $productsQuery->where('name', 'like', '%' . $request->productName . '%');
        }

        $products     = $productsQuery->paginate(10)->withQueryString();
        $currencyCode = (Merchant::active()->first())->currency()->first()->code;

        return Inertia::render(
            'Main',
            [
                'products'     => $products,
                'currencyCode' => $currencyCode,
            ]);
    }
}
