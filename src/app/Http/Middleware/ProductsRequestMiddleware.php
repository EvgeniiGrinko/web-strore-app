<?php

namespace App\Http\Middleware;

use App\Rules\MerchantExist;
use App\Rules\RequestHasAuthKey;
use App\Rules\RequestHasOriginator;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ProductsRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        Log::info('Incoming request: ', [[
            'merchantId' => $request->header('merchantId'),
            'authKey' => $request->header('authKey'),
            'originator' => $request->header('originator'),
        ]]);

        $validator = Validator::make(
            [
                'merchant_id' => $request->header('merchantId'),
                'auth_key' => $request->header('authKey'),
                'originator' => $request->header('originator'),
            ],
            [
                'merchant_id' => ['numeric', 'required', new MerchantExist()],
                'auth_key' => ['string', 'required',  new RequestHasAuthKey()],
                'originator' => ['string', 'required', new RequestHasOriginator()],
            ]
        );

        if ($validator->fails()) {
            return response('Unauthorized', 403);
        }

        return $next($request);
    }
}
