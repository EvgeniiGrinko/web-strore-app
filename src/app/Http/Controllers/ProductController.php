<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    public function __invoke()
    {
        $products = Product::query()->get();

        return ['data' => ($products)];
    }
}
