<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Actions\IndexAction;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Inertia\Inertia;
use Inertia\Response;

class MainController extends Controller
{
    public function seedDB(): RedirectResponse
    {
        Artisan::call('db:seed');

        return redirect()->back();
    }

    public function index(Request $request): Response
    {
        return (new IndexAction())->handle($request);
    }

    public function product(Product $product): Response
    {
        return Inertia::render(
            'Product',
            ['product' => $product]
        );
    }

    public function checkMerchantTransaction(Request $request): JsonResponse
    {
        $orderId = $request->input('order_id');
        $order   = Order::query()->where('id', $orderId)->first();

        return response()->json(
            [
                'transaction_type' => $order->merchantTransaction->type_transaction_code,
                'payment_state'    => $order->merchantTransaction->state,
            ]
        );
    }

    public function privacyPolicy(): Response
    {
        return Inertia::render('PrivacyPolicy');
    }
}
