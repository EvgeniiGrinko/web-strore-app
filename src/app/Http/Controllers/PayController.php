<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Order;
use App\PaymentGateways\PaymentGatewayManager;
use App\Services\PaymentService\Actions\Pay;
use App\Services\PaymentService\Actions\ShowPaymentPage;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;

class PayController extends Controller
{
    public function __construct(public PaymentGatewayManager $paymentDriverManager)
    {
    }

    public function pay(Order $order): RedirectResponse
    {
        return (new Pay())->handle($order);
    }

    public function showPaymentPage(): RedirectResponse|Response
    {
        return (new ShowPaymentPage())->handle();
    }
}
