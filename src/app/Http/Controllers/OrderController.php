<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Models\Order;
use App\Services\OrderService\Actions\CheckPaymentState;
use App\Services\OrderService\Actions\CreateOrder;
use App\Services\OrderService\Actions\Index;
use App\Services\OrderService\Actions\Show;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Response;

class OrderController extends Controller
{
    public function index(): Response
    {
        return (new Index())->handle();
    }

    public function order(Order $order): Response
    {
        return (new Show())->handle($order);
    }

    public function checkPaymentState(Request $request)
    {
        return (new CheckPaymentState())->handle($request->input('order_id'));
    }

    public function createOrder(CreateOrderRequest $request): RedirectResponse
    {
        return (new CreateOrder())->handle($request);
    }
}
