<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\BasketService\Actions\AddItem;
use App\Services\BasketService\Actions\ClearBasket;
use App\Services\BasketService\Actions\DeleteItem;
use App\Services\BasketService\Actions\ShowBasket;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;

class BasketController extends Controller
{
    public function index(): RedirectResponse|Response
    {
        return (new ShowBasket())->handle();
    }

    public function addItem(Product $product): RedirectResponse
    {
        return (new AddItem())->handle($product);
    }

    public function deleteItem(Product $product): RedirectResponse
    {
        return (new DeleteItem())->handle($product);
    }

    public function clear(): RedirectResponse
    {
        return (new ClearBasket())->handle();
    }
}
