<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class APIProductController extends Controller
{
    public function index(Request $request)
    {
        $productsQuery = Product::query();

        if ($request->has('name')) {
            $productsQuery->where('name', $request->get('name'));
        }

        if ($request->has('description')) {
            $productsQuery->where('description', $request->get('description'));
        }

        return $productsQuery->get()->toArray();
    }
}
