<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    public function index(User $user): Response
    {
        return Inertia::render('PayOutPage', ['user' => $user]);
    }
}
