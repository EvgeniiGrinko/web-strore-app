<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ApiServices;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiPayController extends Controller
{
    /**
     * @throws BindingResolutionException
     */
    public function getApiService(Request $request): ApiServices
    {
        return new ApiServices($request);
    }

    /**
     * @throws BindingResolutionException
     */
    public function check(Request $request): JsonResponse
    {
        $apiServices = $this->getApiService($request);

        return $apiServices->checkDataForCheck();
    }

    /**
     * @throws BindingResolutionException
     */
    public function notify(Request $request): JsonResponse
    {
        $apiServices = $this->getApiService($request);

        return $apiServices->checkDataForNotify();
    }
}
