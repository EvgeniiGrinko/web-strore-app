<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\PayoutService\Actions\Payout;
use App\Services\PayoutService\Actions\ShowPayoutPage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Inertia\Response;

class PayoutController
{
    public function payOutInit(Request $request): JsonResponse
    {
        return (new Payout())->handle($request);
    }

    public function payOutView(): Response
    {
        return (new ShowPayoutPage())->handle();
    }
}
