<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class VendorTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_id',
        'payment_id',
        'payment_name',
        'agr_trans_id',
        'merchant_trans_id',
        'merchant_trans_amount',
        'environment',
        'merchant_trans_data',
        'sign_time',
        'sign_string',
        'state',
        'vendor_trans_id',
    ];

    private static int $startingVendorTransId = 1;

    public static function generateVendorTransactionId(): int
    {
        if (!VendorTransaction::query()->latest()->first()) {
            return ++self::$startingVendorTransId;
        } else {
            $last_added_trans = VendorTransaction::latest()->first();

            return ++$last_added_trans->vendor_trans_id;
        }
    }

    public function merchantTransaction(): BelongsTo
    {
        return $this->belongsTo(MerchantTransaction::class, 'merchant_trans_id', 'merchant_trans_id');
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'merchant_trans_id', 'merchant_trans_id');
    }

    public function paymentSystems(): BelongsToMany
    {
        return $this->belongsToMany(
            PaymentSystem::class,
            'payment_system_vendor_transaction',
            'vendor_transaction_id',
            'payment_system_id',
            'vendor_trans_id',
            'payment_id'
        );
    }
}
