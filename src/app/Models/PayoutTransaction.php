<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayoutTransaction extends Model
{
    use HasFactory;

    protected $table = 'payout_transactions';

    protected $fillable = [
        'pg_payment_id',
        'amount',
        'pg_redirect_url',
        'pg_order_id',
        'pg_order_time_limit',
        'pg_description',
        'pg_user_email',
        'pg_status',
        'pg_payment_status',
        'pg_balance_after_payout',
        'pg_to_pay',
        'pg_card_hash',
        'pg_payment_date',
    ];
}
