<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\OrderPaymentStatusEnum;
use App\Enums\OrderStatusEnum;
use App\Exceptions\WebStoreException;
use App\Services\CheckPaymentService;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property int $vendor_id
 * @property string $merchant_trans_id
 * @property int $merchant_trans_amount
 * @property string $merchant_currency
 * @property string $customer_fname
 * @property string $customer_email
 * @property string $phone_number
 * @property int $state
 * @property int|null $user_id
 * @property int $payment_state
 * @property string|Carbon|null $created_at
 * @property string|Carbon|null $updated_at
 * @property Merchant $merchant
 * @property Collection<Merchant> $products
 */
class Order extends Model
{
    use HasFactory;

    protected $statuses = [
        1 => 'Принят в работу',
        2 => 'Выполнен',
        3 => 'Отменен',
    ];

    protected $fillable = [
        'vendor_id',
        'merchant_trans_id',
        'merchant_trans_amount',
        'merchant_currency',
        'product_id',
        'customer_email',
        'customer_fname',
        'phone_number',
        'state',
        'payment_state',
        'currency',
        'sum',
        'type_transaction_code',
        'user_id',
    ];

    /** возвращет связь с имеющимися MerchantTransactions
     * @method Relation merchantTransaction()
     * @return Relation
     */
    public function merchantTransaction(): Relation
    {
        return $this->hasOne(MerchantTransaction::class, 'merchant_trans_id', 'merchant_trans_id');
    }

    /** возвращает связь с имеющимися VendorTransactions
     * @method Relation vendorTransactions()
     * @return Relation
     */
    public function vendorTransactions(): Relation
    {
        return $this->hasMany(VendorTransaction::class, 'merchant_trans_id', 'merchant_trans_id');
    }

    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'vendor_id', 'vendor_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withPivot(['count', 'price', 'discount', "discount_sum", "discount_price"])->withTimestamps();
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /** возвращает сумму заказа
     * @method int getFullSum()
     * @return int
     */
    public function getFullSum(): int
    {
        $sum = 0;
        foreach ($this->products as $product) {
            $sum += $product->price * $product->pivot->count;
        }
        return $sum;
    }

    public function getFullSumWithDiscount(): int|float
    {
        $sum = 0;

        foreach ($this->products as $product) {
            if(isset($product->discount)) {
                $sum += floor(($product->price / 100) * (100 - $product->discount->percentage)) * $product->pivot->count;
            } else {
                $sum += $product->price * $product->pivot->count;
            }
        }

        return $sum;
    }

    public function scopeActive($query): Builder
    {
        return $query->where('state', 1);
    }

    /**
     * @throws WebStoreException
     */
    public function getCurrentStatusDescription(): string
    {
        return OrderStatusEnum::getDescription($this->state);
    }

    /**
     * @throws WebStoreException
     */
    public function getCurrentPaymentStatus(): string
    {
        return OrderPaymentStatusEnum::getDescription($this->payment_state);
    }

    /**
     * @throws GuzzleException
     */
    public function checkPaymentStatusInAgr()
    {
        $resultsForVendorTransactions = [];
        $vendorTransactions = $this->vendorTransactions->toArray();

        for ($i = 0; $i < count($vendorTransactions); $i++) {
            $checkPaymentService = new CheckPaymentService();
            $resultsForVendorTransactions[$vendorTransactions[$i]['agr_trans_id']] =
                $checkPaymentService->checkPaymentByAgrTransId($vendorTransactions[$i]['agr_trans_id']);
        }

        foreach($resultsForVendorTransactions as $response) {
            if ($response->getData()->status == 'Payment status: Успешно оплачено') {
                return 'Оплачено';
            } else {
                return 'Не опплачено';
            }
        }
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
