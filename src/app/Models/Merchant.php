<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property string $auth_key
 * @property int $vendor_id
 * @property string $description
 * @property int $currency_id
 * @property string $secret_key
 * @property string $email
 * @property string $phone
 * @property string $full_name
 * @property string $address
 * @property string $taxpayer_identification_number
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 * @property Currency $currency
 */
class Merchant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'vendor_id',
        'email',
        'phone',
        'full_name',
        'address',
    ];

    public function MerchantTransactions(): Relation
    {
        return $this->hasMany(MerchantTransaction::class, 'vendor_id', 'vendor_id');
    }

    public function Orders(): Relation
    {
        return $this->hasMany(Order::class, 'vendor_id', 'vendor_id');
    }

    public function currency(): Relation
    {
        return $this->belongsToMany(Currency::class);
    }

    public function paymentSystems(): Relation
    {
        return $this->belongsToMany(PaymentSystem::class);
    }

    public function scopeActive(Builder $query): void
    {
        $query->where('active', true);
    }
}
