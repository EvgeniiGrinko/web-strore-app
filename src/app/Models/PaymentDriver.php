<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $code
 * @property int $is_active
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 */
class PaymentDriver extends Model
{
    use HasFactory;
}
