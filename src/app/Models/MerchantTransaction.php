<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string|Carbon|null $created_at
 * @property string|Carbon|null $updated_at
 * @property int $vendor_id
 * @property string $merchant_trans_id
 * @property string|null $merchant_trans_note
 * @property int $merchant_trans_amount
 * @property string $merchant_trans_data
 * @property string|null $request_data
 * @property string|null $response_data
 * @property string|Carbon $sign_time
 * @property string $sign_string
 * @property string|null $pg_status
 * @property string|null $pg_payment_id
 * @property string|null $pg_redirect_url
 * @property string $merchant_currency
 * @property int $order_id
 * @property int $state
 * @property int $type_transaction_code
 */
class MerchantTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_trans_id',
        'merchant_currency',
        'merchant_trans_id',
        'merchant_trans_amount',
        'merchant_trans_data',
        'merchant_trans_note',
        'order_id',
        'sign_time',
        'sign_string',
        'request_data',
        'response_data',
        'state',
        'date',
        'environment',
        'payment_id',
        'state',
        'type_transaction_code',
        'vendor_id',
    ];

    public function order(): Relation
    {
        return $this->belongsTo(Order::class);
    }

    public static function generateTransactionId(int $length = 5): string
    {
        return Str::random($length) . time();
    }

    public function vendorTransactions(): Relation
    {
        return $this->hasMany(VendorTransaction::class, 'merchant_trans_id', 'merchant_trans_id');
    }

    public function transactionType(): Relation
    {
        return $this->belongsTo(PaymentType::class, 'type_transaction_code', 'code');
    }

    public function setRequestData(string|false $requestData): void
    {
        if ($requestData) {
            $this->request_data = $requestData;
        }
    }
}
