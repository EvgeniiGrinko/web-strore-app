<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $code
 * @property int $is_main
 * @property string $name
 * @property string $en_name
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 */
class Currency extends Model
{
    use HasFactory;

    protected $fillable = ['name', "code", 'is_main', 'en_name'];

    public function merchant(): Relation
    {
        return $this->belongsToMany(Merchant::class);
    }
}
