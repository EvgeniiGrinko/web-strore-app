<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property int $percentage
 * @property int $status
 * @property string|Carbon $end_date
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 */
class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'end_date',
        'percentage',
        'product_id',
        'status',
        'current_product_price',
        'new_product_price',
    ];
}
