<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PaymentSystem extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'description'];

    public function vendorTransactions(): BelongsToMany
    {
        return $this->belongsToMany(
            VendorTransaction::class,
            'payment_system_vendor_transaction',
            'payment_system_id',
            'vendor_transaction_id',
            'payment_id',
            'vendor_trans_id'
        );
    }

    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }
}
