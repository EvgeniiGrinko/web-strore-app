<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @param int $id
 * @param string $name
 * @param string $code
 * @param string $description
 * @param int $price
 * @param int $count
 * @param string $image
 * @param int $discount_id
 * @param string $code_pasic
 * @param int $units
 * @param int $vat_percent
 * @param int|float $discount_percentage
 * @param Discount $discount
 * @param string $package_code
 * @param string|Carbon $created_at
 * @param string|Carbon $updated_at
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'description',
        'price',
        'count',
        'code_pasic',
        'units',
        'vat_percent',
        'package_code',
        'image',
    ];

    protected $appends = ['discount_percentage'];

    protected $with = ['discount'];

    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'vendor_id', 'vendor_id');
    }

    public function isAvailable(): bool
    {
        return $this->count > 0;
    }

    public function discount(): HasOne
    {
        return $this->hasOne(Discount::class, 'id', 'discount_id');
    }
    protected function discountPercentage(): Attribute
    {
        return new Attribute(
            get: fn () => $this->discount?->status ? $this->discount?->percentage : 0,
        );
    }

}
