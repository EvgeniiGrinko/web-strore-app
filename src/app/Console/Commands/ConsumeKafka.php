<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Kafka\Handlers\TelegramMessageHandler;
use Illuminate\Console\Command;
use Junges\Kafka\Facades\Kafka;

class ConsumeKafka extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'app:consume-kafka';

    /**
     * The console command description.
     */
    protected $description = 'Consume messages from Kafka';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $consumer = Kafka::createConsumer(topics: ['telegram_message'], groupId: 'group')
            ->subscribe('telegram_message')
            ->withBrokers('kafka:9092')
            ->withOption('debug', 'all')
            ->withConsumerGroupId('group')
            ->withAutoCommit()
            ->withHandler(new TelegramMessageHandler())
            ->withMaxMessages(100000)
            ->build();
        $consumer->consume();
    }
}
