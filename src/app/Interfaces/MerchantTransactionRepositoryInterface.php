<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Models\MerchantTransaction;
use App\Models\Order;

interface MerchantTransactionRepositoryInterface
{
    public function createMerchantTransaction(array $params, Order $order): MerchantTransaction;
}
