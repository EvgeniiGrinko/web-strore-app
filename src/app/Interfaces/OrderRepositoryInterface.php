<?php

namespace App\Interfaces;

use App\Models\Merchant;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    public function createOrder(Merchant $merchant, Collection $products, array $params_from_request): Order;
}
