<?php

declare(strict_types=1);

namespace App\Interfaces;

interface VendorTransactionRepositoryInterface
{
    public function createVendorTransaction(array $vendorPaymentDetails): int;
}
