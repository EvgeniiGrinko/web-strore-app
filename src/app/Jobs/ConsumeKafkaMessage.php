<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Kafka\Handlers\TelegramMessageHandler;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Junges\Kafka\Facades\Kafka;

class ConsumeKafkaMessage implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $consumer = Kafka::createConsumer()
            ->subscribe('telegram_message')
            ->withHandler(new TelegramMessageHandler())
            ->withMaxMessages(1)
            ->build();
        $consumer->consume();
    }
}
