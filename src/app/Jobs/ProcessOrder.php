<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Enums\OrderPaymentStatusEnum;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public int $tries = 5;

    public function __construct(public Order $order)
    {
    }

    public function handle(): void
    {
        if (
            $this->order->payment_state !== OrderPaymentStatusEnum::PAYED->value
            && $this->order->payment_state !== OrderPaymentStatusEnum::CANCELLED->value
            && $this->order->created_at > Carbon::now()->addHour()
        ) {
            $this->order->state = OrderPaymentStatusEnum::CANCELLED->value;
            $this->order->save();

            foreach ($this->order->products as $product) {
                $product->count += $product->pivot->count;
                $product->save();
            }

            Log::channel('billing_process_orders')->info(
                'Automatically cancelled order №' . $this->order->id . ' at - ' . now()
            );
        } else {
            Log::channel('billing_process_orders')->info(
                'Automatically checked payment status of order №' . $this->order->id . ' at - ' . now()
            );
        }
    }
}
