<?php

declare(strict_types=1);

namespace App\Enums;

use App\PaymentGateways\FreedomPay\FreedomPay;

enum PaymentDriversEnum: string
{
    case FREEDOM_PAY = 'freedom_pay';

    public static function getClass(string $code): string
    {
        return match($code) {
            self::FREEDOM_PAY->value => FreedomPay::class,
            default => FreedomPay::class,
        };
    }
}
