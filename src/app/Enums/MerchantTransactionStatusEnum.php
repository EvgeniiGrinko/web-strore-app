<?php

declare(strict_types=1);

namespace App\Enums;

use App\Exceptions\StoreException;

enum MerchantTransactionStatusEnum: string
{
    case SUCCESS   = "m.tr.status.success";
    case CANCELLED = "m.tr.status.cancelled";
    case ERROR     = "m.tr.status.error";
    case CREATED   = "m.tr.status.created";
    case HOLD      = "m.tr.hold";

    /**
     * @throws StoreException
     */
    public static function getCaseValueByCode(int $code): string
    {
        return match ($code) {
            1  => __('merchant_transaction.' . self::SUCCESS->value),
            2  => __('merchant_transaction.' . self::CANCELLED->value),
            -1 => __('merchant_transaction.' . self::ERROR->value),
            0  => __('merchant_transaction.' . self::CREATED->value),
            3  => __('merchant_transaction.' . self::HOLD->value),
            default => throw new StoreException('There is no such Merchant Transaction Status'),
        };
    }

    public function code(): int
    {
        // phpcs:disable
        return match ($this) {
            self::SUCCESS   =>  1,
            self::CANCELLED =>  2,
            self::ERROR     => -1,
            self::CREATED   =>  0,
            self::HOLD      =>  3,
        };
    }

    public function label(string $replaces): string
    {
        return __('merchant_transaction.' . $replaces);
    }
}
