<?php

declare(strict_types=1);

namespace App\Enums;

enum StoreExceptionsEnum: string
{
    case SYSTEM_ERROR_MESSAGE   = "Системная ошибка.";
    case CREATE_PAYMENT_MESSAGE = "Ошибка при создани платежа.";

    public function code(): int
    {
        // phpcs:disable
        return match ($this) {
            self::SYSTEM_ERROR_MESSAGE   => -1,
            self::CREATE_PAYMENT_MESSAGE => -2,
        };
    }
}
