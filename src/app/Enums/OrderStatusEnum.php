<?php

declare(strict_types=1);

namespace App\Enums;

use App\Exceptions\WebStoreException;

enum OrderStatusEnum: int
{
    case IN_PROCESS = 1;
    case COMPLETED  = 2;
    case CANCELLED  = 3;

    public static function getDescription(int $code): string
    {
        return match($code) {
            self::IN_PROCESS->value => 'Принят в работу',
            self::COMPLETED->value  => 'Выполнен',
            self::CANCELLED->value  => 'Отменен',
            default => throw new WebStoreException('There is no such order status')
        };
    }
}
