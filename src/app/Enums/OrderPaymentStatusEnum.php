<?php

declare(strict_types=1);

namespace App\Enums;

use App\Exceptions\WebStoreException;

enum OrderPaymentStatusEnum: int
{
    case NOT_PAYED  = 0;
    case IN_PROCESS = 1;
    case PAYED      = 2;
    case CANCELLED  = 3;

    public static function getDescription(int $code): string
    {
        return match($code) {
            self::NOT_PAYED->value  => 'Не оплачен',
            self::IN_PROCESS->value => 'Оплата в обработке',
            self::PAYED->value      => 'Оплачен',
            self::CANCELLED->value  => 'Оплата отменена',
            default => throw new WebStoreException('There is no such order payment status')
        };
    }
}
