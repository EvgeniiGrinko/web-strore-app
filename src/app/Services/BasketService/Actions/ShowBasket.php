<?php

declare(strict_types=1);

namespace App\Services\BasketService\Actions;

use App\Models\Merchant;
use App\Models\Product;
use App\Services\BasketService\Basket;
use Inertia\Inertia;

class ShowBasket
{
    public function handle()
    {
        if (
            session()->has('basket')
            && !empty(session()->get('basket')['products'])
        ) {
            $products = Product::whereIn('id', array_keys(session()->get('basket')['products']))->get()->toArray();
            $productsWithIds = [];

            foreach($products as $product) {
                $productsWithIds[$product['id']] = $product;
            }
            $basket       = session()->get('basket');
            $basketObject = new Basket();
            $currencyCode = (Merchant::active()->first())->currency()->first()->code;

            return Inertia::render(
                'Basket',
                [
                    'products'           => $productsWithIds,
                    'basket'             => (array) $basket['products'],
                    'fullSum'            => $basketObject->getFullBasketSum(),
                    'fullSumWthDiscount' => $basketObject->getFullBasketSumWthDiscount(),
                    'currencyCode'       => $currencyCode,
                ]
            );
        }
        return redirect()->route('index')->with('success', __('basket.is.empty'));
    }
}
