<?php

declare(strict_types=1);

namespace App\Services\BasketService\Actions;

use App\Models\Product;
use Illuminate\Support\Facades\Route;

class AddItem
{
    public function handle(Product $product)
    {
        if (
            !session()->has('basket')
            && empty(session()->get('basket')['products'])
            && Route::currentRouteName() === "basket"
            && app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() === "basket"
        ) {
            return redirect()->route('index')->with('success', 'Ваша корзина пуста');
        }

        $basket = session()->get('basket');
        if(empty($basket['products']) && ($product->count > 1)) {
            session(['basket' => [
                'products' =>
                    [
                        $product->id => 1,
                    ],
            ]]);
            $basket = session()->get('basket');
        } else {
            if(array_key_exists($product->id, $basket['products']) && ($product->count > 1)) {
                $basket['products'][$product->id] = $basket['products'][$product->id] + 1;
            } elseif ($product->count > 1) {
                $basket['products'][$product->id] = 1;
            }
            session(['basket' => $basket]);

        }

        return redirect()->route('basket');
    }

}
