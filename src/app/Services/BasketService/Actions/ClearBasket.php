<?php

declare(strict_types=1);

namespace App\Services\BasketService\Actions;


use App\Services\BasketService\Basket;
use Illuminate\Http\RedirectResponse;

class ClearBasket
{
    public function handle(): RedirectResponse
    {
        $basketObject = new Basket();
        $basketObject->clearBasket();

        return redirect()->route('index');
    }
}
