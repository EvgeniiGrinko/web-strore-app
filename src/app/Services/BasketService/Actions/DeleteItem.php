<?php

declare(strict_types=1);

namespace App\Services\BasketService\Actions;

use App\Models\Product;
use Illuminate\Http\RedirectResponse;

class DeleteItem
{

    public function handle(Product $product): RedirectResponse
    {

        if (
            !session()->has('basket')
            && empty(session()->get('basket')['products'])
            && Route::currentRouteName() === "basket"
            && app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() === "basket"
        ) {
            return redirect()->route('index')->with('success', 'Ваша корзина пуста');
        }

        $basket = session()->get('basket');
        if (
            array_key_exists($product->id, $basket['products'])
            && ($basket['products'][$product->id] > 1)
        ) {
            $basket['products'][$product->id] = $basket['products'][$product->id] - 1;
        } elseif (
            array_key_exists($product->id, $basket['products'])
            && ($basket['products'][$product->id] == 1)
        ) {
            unset($basket['products'][$product->id]);
        }

        session(['basket' => $basket]);
        if (empty($basket['products'])) {
            return redirect()->route('index');
        }

        return redirect()->back();
    }
}
