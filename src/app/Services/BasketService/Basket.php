<?php

declare(strict_types=1);

namespace App\Services\BasketService;

use App\Models\Product;

class Basket
{
    protected array $basket;

    public function __construct()
    {
        $this->basket = session()->get('basket');
    }

    public function getFullBasketSum(): int|float
    {
        $basket = session()->get('basket');
        $sum = 0;
        $products = Product::whereIn('id', array_keys($basket['products']))->get();

        foreach ($basket['products'] as $productId => $productCount) {
            $sum += $products->find($productId)->price * $productCount;
        }

        return $sum;
    }

    public function getFullBasketSumWthDiscount(): int|float
    {
        $basket = session()->get('basket');
        $sum = 0;
        $products = Product::whereIn('id', array_keys($basket['products']))->get();

        foreach ($basket['products'] as $productId => $productCount) {
            if (isset($products->find($productId)->discount)) {
                $sum += (($products->find($productId)->price / 100) * (100 - $products->find($productId)->discount->percentage)) * $productCount;
            } else {
                $sum += $products->find($productId)->price * $productCount;
            }
        }

        return $sum;
    }

    public function clearBasket(): void
    {
        session()->forget('basket');
    }
}
