<?php

namespace App\Services\RequestChecks;

use App\Models\VendorTransaction;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * RequestVendorTransactionExistsCheck служит для проверки существования транзакции
 * @used-by ApiServices
 * @author  Evgenii Grinko <evgeniigrinkopsp@gmail.com>
 */
class RequestVendorTransactionExistsCheck
{
    /** Вовзращает транзакцию (VendorTransaction) если она есть в БД
     * @method check()
     * @param int $trans_id - это число либо vendor_trans_id, либо agr_trans_id в зависимости от мемтода в ApiServices
     * @param string $flag - это строка либо vendor_trans_id, либо agr_trans_id в зависимости от мемтода в ApiServices
     * @return VendorTransaction
     * @throws Exception с кодом -6 если транзакция не в БД
     */
    public static function check(int $trans_id, string $flag): VendorTransaction
    {
        try {
            if($flag === 'vendor_trans_id') {
                return VendorTransaction::where('vendor_trans_id', $trans_id)->firstOrFail();
            } else {
                return VendorTransaction::where('agr_trans_id', $trans_id)->firstOrFail();
            }
        } catch (ModelNotFoundException $exception) {
            throw new \Exception('Err', -6);
        }
    }
}
