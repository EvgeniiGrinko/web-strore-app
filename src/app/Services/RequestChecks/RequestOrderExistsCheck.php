<?php

declare(strict_types=1);

namespace App\Services\RequestChecks;

use App\Models\Order;
use App\Repositories\OrderRepository;
use Exception;
use Illuminate\Http\Request;

class RequestOrderExistsCheck
{
    /** Вовзращает заказ если он найден
     * @method check()
     * @param Request $request
     * @param string $methodName название метода на английском с большой буквы
     * @return Order
     * @throws Exception с кодом -5 если заказ не найден
     */
    public static function check(Request $request, string $methodName): Order
    {
        $data = $request->all();
        $flag = null;
        $order = null;

        if ((($methodName === 'Info') || ($methodName === 'Pay')) && !empty($data["MERCHANT_TRANS_ID"])) {
            $flag = (new OrderRepository())->findOrder($data["MERCHANT_TRANS_ID"]) ?? false;
            $order = (new OrderRepository())->findOrder($data["MERCHANT_TRANS_ID"]);
        } elseif ((($methodName === 'Notify') || ($methodName === 'Cancel')) && !empty($data['VENDOR_TRANS_ID'])) {
            $flag = (new OrderRepository())->findOrderByVendorTransactionId($data['VENDOR_TRANS_ID']) ?? false;
            $order = (new OrderRepository())->findOrderByVendorTransactionId($data['VENDOR_TRANS_ID']);
        } else {
            $flag = false;
        }

        if (!$flag) {
            throw new \Exception('Err', -5);
        } else {
            return $order;
        }
    }
}
