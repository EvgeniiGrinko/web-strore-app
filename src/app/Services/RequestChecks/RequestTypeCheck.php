<?php

namespace App\Services\RequestChecks;

use Exception;
use Illuminate\Http\Request;

/** Служит для проверки типа запроса
 * @author  Evgenii Grinko <evgeniigrinkopsp@gmail.com>
 * @used-by ApiServices
 */
class RequestTypeCheck
{
    /** Проверяет тип запроса JSON, если нет то выбрасывает исключение
     * @return bool возвращает true усли тип запроса JSON
     * @param Request $request
     * @throws Exception с кодом ошибки -8 'Error in request', если тип запроса не JSON
     */
    public static function check(Request $request)
    {
        if ($request->getContentType() !== 'json') {
            throw new Exception('Err', -8);
        } else {
            return true;
        }
    }
}
