<?php

namespace App\Services\RequestChecks;

use App\Models\Order;
use Exception;

/**
 * RequestOrderPaidCheck служит для проверки статуса заказа
 * @used-by ApiServices
 * @author  Evgenii Grinko <evgeniigrinkopsp@gmail.com>
 */
class RequestOrderPaidCheck
{
    /** Вовзращает true если платежный статус заказа не 2 (оплачен)
     * @method check()
     * @param Order $order
     * @return bool
     * @throws Exception с кодом -4 если статус заказа 2 (оплачен)
     */
    public static function check(Order $order)
    {
        if ($order->payment_state === 2) {
            //if ($order->payment_state === 1)
            throw new Exception('Err', -4);
        } else {
            return true;
        }
    }
}
