<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\VendorTransactionRepository;
use App\Services\RequestChecks\RequestOrderExistsCheck;
use App\Services\RequestChecks\RequestOrderPaidCheck;
use App\Services\RequestChecks\RequestVendorTransactionExistsCheck;
use App\Services\RequestChecks\RequestTypeCheck;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiServices
{
    public array $status_codes;
    public array $data;
    public VendorTransactionRepository $vendorTransactionRepository;

    public function __construct(public Request $request)
    {
        $this->status_codes = [
            0 => response()->json([
                "ERROR" => 0,
                "ERROR_NOTE" => 'Success',
                ]),
            -1 => response()->json(
                [
                    "ERROR" => -1,
                    "ERROR_NOTE" => 'SIGN CHECK FAILED!',
                ]
            ),
            -2 => response()->json(
                [
                    "ERROR" => -2,
                    "ERROR_NOTE" => "Incorrect parameter amount",
                ]
            ),
            -3 => response()->json(
                [
                    "ERROR" => -3,
                    "ERROR_NOTE" => "Not enough parameters",
                ]
            ),
            -4 => response()->json(
                [
                    "ERROR" => -4,
                    "ERROR_NOTE" => "Already paid",
                ]
            ),
            -5 => response()->json(
                [
                    "ERROR" => -5,
                    "ERROR_NOTE" => "The order does not exist",
                ]
            ),
            -6 => response()->json(
                [
                    "ERROR" => -6,
                    "ERROR_NOTE" => 'The transaction does not exist',
                ]
            ),
            -7 => response()->json(
                [
                    "ERROR" => -7,
                    "ERROR_NOTE" => "Failed to update user",
                ]
            ),
            -8 => response()->json(
                [
                    "ERROR" => -8,
                    "ERROR_NOTE" => 'Error in request',
                ]
            ),
            -9 => response()->json(
                [
                    "ERROR" => -9,
                    "ERROR_NOTE" => 'Transaction cancelled',
                ]
            ),
            -10 => response()->json(
                [
                    "ERROR" => -10,
                    "ERROR_NOTE" => "The vendor is not found",
                ]
            ),
            -11 => response()->json(
                [
                    "ERROR" => -11,
                    "ERROR_NOTE" => "Transaction type is not correct",
                ]
            ),
        ];

        $this->data = $request->all();

        $this->vendorTransactionRepository = new VendorTransactionRepository();
    }

    public function checkDataForCheck(): JsonResponse
    {
        $processId = LoggingService::logRequest($this->request, 'Info');
        $result = $this->status_codes[-1];
        try {
            /** Проверяем тип запроса на JSON (при несоответствии - исключение с кодом -8 'Error in request')*/
            RequestTypeCheck::check($this->request);


            /** Проверяем подпись (при несоответствии - исключение с кодом -1 'SIGN CHECK FAILED!') */
            CheckSignString::check(
                $this->data['SIGN_STRING'],
                $this->data['MERCHANT_TRANS_ID'],
                $this->data['SIGN_TIME']
            );

            /**
             * Записываем order (при отстуствии заказа в БД - исключение с кодом
             * -5 "The order does not exist")
             */
            $order = RequestOrderExistsCheck::check($this->request, 'Info');

            /** если Order payment_state равен 2 (оплачен) - исключение с кодом -4 "Already paid" */
            RequestOrderPaidCheck::check($order);

            /**
             * Параметры для ответа
             */
            $parameters = [];
            $parameters['full_name'] = $order->customer_fname;
            $parameters['phone'] = $order->phone_number;
            $parameters['order_id'] = $order->customer_email;

            $result = response()->json(
                [
                    "ERROR" => 0,
                    "ERROR_NOTE" => "Success",
                    "PARAMETERS" => $parameters,
                ]
            );
            return $result;
        } catch (Exception $exception) {

            /** Логируем исключение и отправляем Response по коду ошибки */
            LoggingService::logException($processId, $exception, 'Info');
            $result = $this->status_codes[$exception->getCode()];
            return $result ?? $this->status_codes[-1];

        } finally {
            /** логируем $result */
            LoggingService::logResponse($result, 'Info', $processId);
        }
    }

    public function checkDataForNotify(): JsonResponse
    {
        $processId = LoggingService::logRequest($this->request, 'Notify');
        $result = $this->status_codes[-1];
        try {
            /** Проверяем тип запроса на JSON (при несоответствии - исключение с кодом -8 'Error in request')*/
            RequestTypeCheck::check($this->request);

            /** Проверяем подпись (при несоответствии - исключение с кодом -1 'SIGN CHECK FAILED!') */
            CheckSignString::check(
                $this->data['SIGN_STRING'],
                $this->data['AGR_TRANS_ID'],
                $this->data['VENDOR_TRANS_ID'],
                $this->data['STATUS'],
                $this->data['SIGN_TIME']
            );

            /** Находим vendor_transaction, при отсутствии в БД - исключение с кодом -6 'The transaction does not exist' */
            $vendor_transaction = RequestVendorTransactionExistsCheck::check(
                $this->data['VENDOR_TRANS_ID'],
                'vendor_trans_id'
            );

            /** Обновляем статус у VendorTransaction, merchantTransaction, order */
            $vendor_transaction->update(['state' => $this->data["STATUS"]]);
            $vendor_transaction->merchantTransaction->update(['state' => $this->data['STATUS']]);
            $vendor_transaction->order->update(['payment_state' => $this->data['STATUS']]);
            $result = $this->status_codes[0];
            return $result;

        } catch (Exception $exception) {

            /** Логируем исключение и отправляем Response по коду ошибки */
            LoggingService::logException($processId, $exception, 'Notify');
            $result = $this->status_codes[$exception->getCode()];
            return $result;

        } finally {
            /** логируем $result*/
            LoggingService::logResponse($result, 'Notify', $processId);
        }
    }
}
