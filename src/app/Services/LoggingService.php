<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Throwable;

class LoggingService
{
    public static function logRequest(Request $request, string $methodName): string
    {
        $processId = Str::random(5) . time();

        ($request->getContentType() === 'json') ? Log::channel('billing')->info($methodName .' request: ' . $processId . ' : ', $request->all())
            : Log::channel('billing')->info($methodName . ' request № ' . $processId . ' : ', [$request->getContent()]);
        return $processId;
    }

    public static function logResponse(JsonResponse $response, string $methodName, string $processId): void
    {
        Log::channel('billing')->info('For Request № ' . $processId . ' '. $methodName . ' response: ', [$response->getData()]);
    }

    public static function logException(string $processId, Throwable $exception, string $methodName): void
    {
        Log::channel('billing_exceptions')->error(
            $methodName . ' - exception' . ' request № : ' . $processId . ' ',
            [
                'Message' => $exception->getMessage(),
                'Code' => $exception->getCode(),
                'File' => $exception->getFile(),
                'Line' => $exception->getLine(),
                'Previous' => $exception->getPrevious(),
                'Trace as string' => $exception->getTraceAsString(),
            ]
        );
    }

    /** @param array<mixed> $params */
    public static function logCheckoutParams(array $params): void
    {
        Log::channel('billing')->info('Checkout request ', [$params]);
    }

    /** @param array<mixed> $paramsForCheck */
    public static function logCheckStatusRequest(array $paramsForCheck): string
    {
        $processId = Str::random(5) . time();
        Log::channel('billing_status')->info('Params for check payment status № ' . $processId, [$paramsForCheck]);
        return $processId;
    }

    public static function logCheckStatusResponse(mixed $paramsForCheck, string $processId): void
    {
        Log::channel('billing_status')->info('Response for check № ' . $processId . ' payment status ', [$paramsForCheck]);
    }
}
