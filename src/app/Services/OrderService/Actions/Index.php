<?php

declare(strict_types=1);

namespace App\Services\OrderService\Actions;

use App\Models\Order;
use Inertia\Inertia;
use Inertia\Response;

class Index
{
    public function handle(): Response
    {
        $orders = Order::query()->paginate(5);

        return Inertia::render(
            'OrdersPage',
            ['orders' => $orders]
        );
    }
}
