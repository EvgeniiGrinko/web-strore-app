<?php

declare(strict_types=1);

namespace App\Services\OrderService\Actions;

use App\Facades\Telegram;
use App\Http\Requests\CreateOrderRequest;
use App\Jobs\ProcessOrder;
use App\Models\Merchant;
use App\Models\Order;
use App\Models\Product;
use App\Repositories\OrderRepository;
use Illuminate\Http\RedirectResponse;

class CreateOrder
{
    public function handle(CreateOrderRequest $request): RedirectResponse
    {
        $message = null;
        $params_from_request = $request->all();
        $merchant = Merchant::first();

        if (
            is_null(session()->get('basket'))
            || is_null(session()->get('basket')['products'])
        ) {
            return redirect()->route('index');
        }

        $products = Product::query()->whereIn('id', array_keys(session()->get('basket')['products']))->get();
        $unavailable_products = [];

        foreach ($products as $product) {
            if(($product->count - session()->get('basket')['products'][$product->id]) < 0) {
                $unavailable_products[] = $product;
            }
        }

        if (count($unavailable_products) > 0) {
            $message = 'Следующие товары недоступны для заказа в требуемом количестве: ';
            foreach($unavailable_products as $product) {
                $message .= $product->name . ' ';
            }
        }

        $orderRepository = new OrderRepository();
        $order = $message ?: $orderRepository->createOrder($merchant, $products, $params_from_request);

        if (is_string($order)) {
            session()->flash('products_unavailable', $order);
            return redirect()->route('basket')->with('products_unavailable', $order);
        } elseif ($order instanceof  Order) {
            ProcessOrder::dispatch($order)->delay(now()->addMinutes(60));
        }

        try {
            (new Telegram())->reportOrderCreated($order);
        } catch(\Throwable $t) {}

        session(['orderId' => $order->id]);
        session()->forget('basket');
        session()->forget('basketObject');

        return redirect()->route('show.payment.page');
    }
}
