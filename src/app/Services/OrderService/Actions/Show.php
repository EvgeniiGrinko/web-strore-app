<?php

declare(strict_types=1);

namespace App\Services\OrderService\Actions;

use App\Models\Order;
use Inertia\Inertia;

class Show
{
    public function handle(Order $order)
    {
        $products = $order->products()->get();

        return Inertia::render('OrderPage', [
            'order'                 => $order,
            'products'              => $products,
            'full_sum'              => $order->getFullSum(),
            'full_sum_wth_discount' => $order->getFullSumWithDiscount(),
            'status_description'    => $order->getCurrentStatusDescription(),
            'payment_status'        => $order->getCurrentPaymentStatus(),
        ]);
    }
}
