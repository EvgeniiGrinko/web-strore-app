<?php

declare(strict_types=1);

namespace App\Services\OrderService\Actions;

use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CheckPaymentState
{
    public function handle(int $orderId)
    {
        $order = null;

        try {
            $order = Order::findOrFail($orderId);
        } catch (ModelNotFoundException $exception) {
            return response()->json(["status" => 'Ошибка']);
        }

        return $order->checkPaymentStatusInAgr();
    }
}
