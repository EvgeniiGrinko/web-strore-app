<?php

declare(strict_types=1);

namespace App\Services\PaymentService\Actions;

use App\Models\Order;
use Illuminate\Http\RedirectResponse;
use App\PaymentGateways\Facades\Payment;
use Illuminate\Support\Facades\Log;

class Pay
{
    public function handle(Order $order): RedirectResponse
    {
        $resStatus = null;
        try {
            $merchantTransaction =  Payment::driver('freedomPay')->pay($order);
            $resStatus = $merchantTransaction->pg_status;
        } catch (\Throwable $t) {
            Log::error("Error", [
                $t
            ]);
            $resStatus = 'error';
        }
        if (
            empty($merchantTransaction)
            || empty($merchantTransaction->pg_redirect_url)
            || $merchantTransaction->pg_status === 'error'
            || $resStatus === 'error'
        ) {
            return redirect()->route('index')->with('error', 'Error occurred paying for order № ' . $order->id);
        }

        return redirect()->away($merchantTransaction->pg_redirect_url);
    }
}
