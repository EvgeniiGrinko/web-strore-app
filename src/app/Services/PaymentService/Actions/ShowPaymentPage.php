<?php

declare(strict_types=1);

namespace App\Services\PaymentService\Actions;

use App\Models\Merchant;
use App\Models\Order;
use App\Models\PaymentType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Inertia\Inertia;
use Inertia\Response;

class ShowPaymentPage
{
    public function handle(): RedirectResponse|Response
    {
        $order           = Order::where('id', session()->get('orderId'))->first();
        $paymentTypes    = PaymentType::get();
        $products        = $order?->products ?? new Collection([]);
        $productsWithIds = [];

        foreach($products as $product) {
            $productsWithIds[$product['id']] = $product;
        }

        $currencyCode = (Merchant::active()->first())->currency()->first()->code;

        if($order) {
            return Inertia::render('PaymentPage', [
                'order'              => $order,
                'products'           => $productsWithIds,
                'fullSum'            => $order->getFullSum(),
                'fullSumWthDiscount' => $order->getFullSumWithDiscount(),
                'paymentTypes'       => $paymentTypes,
                'currencyCode'       => $currencyCode,
            ]);
        } else {
            return redirect()->route('index');
        }
    }
}
