<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Merchant;
use Exception;

class CheckSignString
{
    public static $merchants;

    public static function check(string $signString, ...$args)
    {
        self::$merchants = Merchant::get();
        $result = [];
        foreach (self::$merchants as $merchant) {
            $result[$merchant->vendor_id] = ($signString === md5($merchant->secret_key . implode('', $args)));
        }

        if(in_array(true, $result, true)) {
            return true;
        } else {
            throw new Exception('Err', -1);
        }
    }
}
