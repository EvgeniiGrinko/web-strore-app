<?php

declare(strict_types=1);

namespace App\Services\PayoutService\Actions;

use Inertia\Inertia;
use Inertia\Response;

class ShowPayoutPage
{
    public function handle(): Response
    {
        return Inertia::render('PayOutPage');
    }
}
