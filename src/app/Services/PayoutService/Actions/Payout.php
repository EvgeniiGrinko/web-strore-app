<?php

declare(strict_types=1);

namespace App\Services\PayoutService\Actions;

use App\PaymentGateways\Facades\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Payout
{
    public function handle(Request $request)
    {
        $resp = null;
        try {
            $resp = Payment::driver('freedomPay')->payOut($request->payout_sum);
            Log::info('Response from FreedomPay for payout', [$resp]);
        } catch (\Throwable $t) {
            Log::error('Error Response from FreedomPay for payout', [
                'code' => $t->getCode(),
                'message' => $t->getMessage(),
            ]);
        } finally {
            if (
                !empty($resp)
                && $resp->pg_status === 'error'
            ) {
                return response()->json([
                    'error' => __('index.request.error'),
                ]);
            }
        }

        return response()->json([
            'pg_redirect_url' => (string) $resp->pg_redirect_url,
            'pg_status' =>  (string) $resp->pg_status,
        ]);
    }
}
