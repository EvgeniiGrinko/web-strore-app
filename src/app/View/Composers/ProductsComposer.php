<?php

declare(strict_types=1);

namespace App\View\Composers;

use App\Models\Product;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class ProductsComposer
{
    public function compose(View $view): void
    {
        $products = Product::query()->paginate(6)->withQueryString();
        $view->with('products', $products);
        $view->with('canLogin', Route::has('login'));
        $view->with('canRegister', Route::has('register'));
    }
}
