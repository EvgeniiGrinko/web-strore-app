<?php

declare(strict_types=1);

namespace App\Rules;

use App\Models\Merchant;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class MerchantExist implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!Merchant::find($value)) {
            $fail('The :attribute must be id of Merchant.');
        }
    }
}
