<?php

declare(strict_types=1);

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class RequestHasOriginator implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $originators = [
            'merchant',
            'partner',
            'owner',
        ];

        if (!in_array(strtolower($value), $originators, true)) {
            $fail('The :attribute must be from the list of trustable request originators.');
        }
    }
}
