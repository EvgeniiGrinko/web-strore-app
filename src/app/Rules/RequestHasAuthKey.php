<?php

declare(strict_types=1);

namespace App\Rules;

use App\Models\Merchant;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class RequestHasAuthKey implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $merchant = Merchant::find(request()->header('merchantId'));

        if (empty($merchant->auth_key) || $value !== $merchant->auth_key) {
            $fail('The :attribute must be uppercase.');
        }
    }
}
