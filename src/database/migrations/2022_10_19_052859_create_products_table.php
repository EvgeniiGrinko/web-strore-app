<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->text('description');
            $table->integer('price');
            $table->integer('count');
            $table->string("image");
            $table->integer("discount_id")->nullable();
            $table->string("code_pasic");
            $table->integer("units")->nullable();
            $table->integer("vat_percent")->nullable();
            $table->string("package_code")->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
