<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->string('merchant_trans_id');
            $table->integer('merchant_trans_amount');
            $table->string('merchant_currency')->default('sum');
            $table->string('customer_fname');
            $table->string('customer_email');
            $table->string('phone_number');
            $table->integer('state');
            $table->integer("user_id")->nullable();
            $table->integer('payment_state')->default(0);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
