<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('payment_systems', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->string('description');
            $table->integer('payment_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('payment_systems');
    }
};
