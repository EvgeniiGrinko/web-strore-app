<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('auth_key');
            $table->integer('vendor_id');
            $table->text('description')->nullable();
            $table->integer('currency_id');
            $table->boolean('active')->default(false);
            $table->string("secret_key")->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('full_name');
            $table->string('address');
            $table->string('taxpayer_identification_number');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('merchants');
    }
};
