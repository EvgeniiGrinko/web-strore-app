<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('currency_merchant', function (Blueprint $table) {
            $table->id();
            $table->integer('currency_id');
            $table->integer('merchant_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('currency_merchant');
    }
};
