<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('merchant_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->string('merchant_trans_id')->unique();
            $table->string('merchant_trans_note')->nullable();
            $table->integer('merchant_trans_amount');
            $table->string('merchant_trans_data');
            $table->text('request_data')->nullable();
            $table->text('response_data')->nullable();
            $table->dateTime('sign_time', 3);
            $table->string('sign_string');
            $table->string('pg_status')->nullable()->default(null);
            $table->string('pg_payment_id')->nullable()->default(null);
            $table->string('pg_redirect_url')->nullable()->default(null);
            $table->string('merchant_currency');
            $table->integer('order_id');
            $table->integer('state');
            $table->integer('type_transaction_code');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('merchant_transactions');
    }
};
