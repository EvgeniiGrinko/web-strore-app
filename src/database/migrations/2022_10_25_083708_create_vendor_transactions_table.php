<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('vendor_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->integer('payment_id');
            $table->string('payment_name');
            $table->unsignedBigInteger('agr_trans_id');
            $table->string('merchant_trans_id');
            $table->integer('merchant_trans_amount');
            $table->string('environment');
            $table->string('merchant_trans_data')->nullable();
            $table->dateTime('sign_time', 3);
            $table->string('sign_string');
            $table->integer('state')->nullable();
            $table->integer('vendor_trans_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('vendor_transactions');
    }
};
