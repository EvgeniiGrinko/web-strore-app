<?php

declare(strict_types=1);

namespace database\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration
{
    public function up(): void
    {
        Schema::create('payment_system_vendor_transaction', function (Blueprint $table) {
            $table->id();
            $table->integer('payment_system_id');
            $table->integer('vendor_transaction_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('payment_system_vendor_transaction');
    }
};
