<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'Grinko Evgenii',
                'email' => 'evgeniigrinkopsp@gmail.com',
                'password' => bcrypt('12345678'),
                'is_admin' => 1,
                'phone_number' => '998880866001',
                'address' => 'Tashkent, Bunyodkor st, 15/65',
            ],
            [
                'name' => 'Гринько Евгений',
                'email' => 'grinkoevgenii77@gmail.com',
                'password' => bcrypt('12345678'),
                'is_admin' => 0,
                'phone_number' => '998880862101',
                'address' => 'Tashkent, Mirzo Ulugbek st, 20/17',
            ],
        ]);
    }
}
