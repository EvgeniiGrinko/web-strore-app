<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountsSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('discounts')->truncate();
        DB::table('discounts')->insert(
            [
            ['end_date' => '2022-12-08 06:15:15', 'percentage' => 20, 'status' => 1],
            ['end_date' => '2022-12-28 06:15:15', 'percentage' => 15, 'status' => 1],
            ['end_date' => '2023-01-08 06:15:15', 'percentage' => 25, 'status' => 1],
          ]
        );
    }
}
