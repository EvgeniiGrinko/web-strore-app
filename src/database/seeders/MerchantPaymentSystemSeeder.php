<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MerchantPaymentSystemSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('merchant_payment_system')->truncate();
        DB::table('merchant_payment_system')->insert(
            [
                [
                    'merchant_id' => 1,
                    'payment_system_id' => 1,
                ],
                [
                    'merchant_id' => 1,
                    'payment_system_id' => 2,
                ],
                [
                    'merchant_id' => 1,
                    'payment_system_id' => 3,
                ],
            ]
        );
    }
}
