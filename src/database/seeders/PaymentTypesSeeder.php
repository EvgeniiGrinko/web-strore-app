<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypesSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('payment_types')->truncate();
        DB::table('payment_types')->insert([[
            'ru_name' => 'Покупка',
            'en_name' => 'purchase',
            'code' => 0,
        ],
            [
            'ru_name' => 'Аванс',
            'en_name' => 'prepayment',
            'code' => 1,
        ],
            [
            'ru_name' => 'Кредит',
            'en_name' => 'credit',
            'code' => 2,
        ]]);
    }
}
