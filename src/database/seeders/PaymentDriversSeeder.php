<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Enums\PaymentDriversEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentDriversSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('payment_drivers')->truncate();
        DB::table('payment_drivers')->insert([
            'is_active' => 1,
            'code'      => PaymentDriversEnum::FREEDOM_PAY->value,
        ]);
    }
}
