<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('currencies')->truncate();
        DB::table('currencies')->insert([
            'name' => 'Киргизский сум',
            'en_name' => 'sum',
            'code' => 'KGS',
            'is_main' => 1,
        ]);
    }
}
