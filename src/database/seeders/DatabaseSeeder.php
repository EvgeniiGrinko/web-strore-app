<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(ProductsSeeder::class);
        $this->call(MerchantSeeder::class);
        $this->call(PaymentSystemsSeeder::class);
        $this->call(CurrenciesSeeder::class);
        $this->call(CurrencyMerchantSeeder::class);
        $this->call(MerchantPaymentSystemSeeder::class);
        $this->call(DiscountsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(PaymentTypesSeeder::class);
        $this->call(PaymentDriversSeeder::class);
    }
}
