<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MerchantSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('merchants')->truncate();
        DB::table('merchants')->insert([
            'vendor_id'                      => 101599,
            'name'                           => 'Billing True',
            'description'                    => 'We build billing systems',
            'currency_id'                    => 1,
            'active'                         => 1,
            'taxpayer_identification_number' => '308637743',
            'secret_key'                     => '_mug271pO0uN!=C_c0uPJF8X4VvCcv+d',
            'email'                          => 'info@psp.uz',
            'phone'                          => '+998909014792',
            'full_name'                      => 'Billing Systems, JSC',
            'address'                        => 'Tashkent, 5th str Sayram, 92',
            'auth_key'                       => 'wkjautiqgueidtwgiqblDKY98',
        ]);
    }
}
