<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSystemsSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('payment_systems')->truncate();
        DB::table('payment_systems')->insert([[
            'name' => 'Uzbekistan card',
            'code' => 'uzcard',
            'description' => 'Uzcard работает только на территории Узбекистана',
            'payment_id' => 9,
        ],
        [
            'name' => 'Humo card',
            'code' => 'humo',
            'description' => 'Humo работает только на территории Узбекистана',
            'payment_id' => 16,
        ],
        [
            'name' => 'Click',
            'code' => 'click',
            'description' => 'Click - это система мобильного банкинга, позволяющая производить оплату с мобильного телефона
                 за услуги сотовых операторов, интернет-провайдеров и других компаний',
            'payment_id' => 1,
        ],]);
    }
}
