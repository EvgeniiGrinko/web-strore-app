<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencyMerchantSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('currency_merchant')->truncate();
        DB::table('currency_merchant')->insert([
            'currency_id' => 1,
            'merchant_id' => 1,
        ]);
    }
}
