<?php

declare(strict_types=1);

return [
    "server.response.error" => "Възникна грешка при изпълнение на заявката",
];
