<?php

declare(strict_types=1);

return [
    "m.tr.status.success"   => "Успех",
    "m.tr.status.cancelled" => "Отменен",
    "m.tr.status.error"     => "Грешка",
    "m.tr.status.created"   => "Създаден",
    "m.tr.status.hold"      => "Задръжте",
];
