<?php

declare(strict_types=1);

return [
    "basket.is.empty" => "Your Basket is Empty",
];
