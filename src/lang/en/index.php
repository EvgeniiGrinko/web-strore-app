<?php

declare(strict_types=1);

return [
    "server.response.error" => "An error occurred while executing the request",
];
