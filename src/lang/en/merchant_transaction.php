<?php

declare(strict_types=1);

return [
    "m.tr.status.success"   => "Success",
    "m.tr.status.cancelled" => "Cancelled",
    "m.tr.status.error"     => "Error",
    "m.tr.status.created"   => "Created",
    "m.tr.status.hold"      => "Hold",
];
