<?php

declare(strict_types=1);

return [
    "m.tr.status.success"   => "Con successo",
    "m.tr.status.cancelled" => "Abrogato",
    "m.tr.status.error"     => "Errore",
    "m.tr.status.created"   => "Creato",
    "m.tr.status.hold"      => "Regge",
];
