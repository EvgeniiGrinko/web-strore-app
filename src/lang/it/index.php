<?php

declare(strict_types=1);

return [
    "server.response.error" => "Si è verificato un errore durante l'esecuzione della query",
];
