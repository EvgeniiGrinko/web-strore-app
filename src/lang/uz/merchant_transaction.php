<?php

declare(strict_types=1);

return [
    "m.tr.status.success"   => "Muvaffaqiyat",
    "m.tr.status.cancelled" => "Bekor qilindi",
    "m.tr.status.error"     => "Xato",
    "m.tr.status.created"   => "Yaratilgan",
    "m.tr.status.hold"      => "Ushlab turilgan",
];
