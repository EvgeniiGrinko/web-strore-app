<?php

declare(strict_types=1);

return [
    "m.tr.status.success"   => "Успешно",
    "m.tr.status.cancelled" => "Отменено",
    "m.tr.status.error"     => "Ошибка",
    "m.tr.status.created"   => "Создано",
    "m.tr.status.hold"      => "Удерживается",
];
