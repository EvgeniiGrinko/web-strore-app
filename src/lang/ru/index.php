<?php

declare(strict_types=1);

return [
    "server.response.error" => "Произошла ошибка при выполнении запроса",
];
