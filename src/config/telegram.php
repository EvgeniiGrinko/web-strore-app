<?php

declare(strict_types=1);

return [
    'api_endpoint'       => 'https://api.telegram.org/bot',
    'report_chat_id'     => env('CHAT_ID_TO_REPORT_BY_TELEGRAM_BOT'),
    'telegram_bot_token' => env('TELEGRAM_BOT_TOKEN'),
];
