<?php

declare(strict_types=1);

return [
    'freedom' => [
        'freedompay_merchant_id'            => env('FREEDOMPAY_MERCHANT_ID'),
        'freedompay_merchant_secret'        => env('FREEDOMPAY_MERCHANT_SECRET_PAYIN'),
        'freedompay_merchant_secret_payout' => env('FREEDOMPAY_MERCHANT_SECRET_PAYOUT'),
    ],
];
