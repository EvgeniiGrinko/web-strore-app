<?php

use App\PaymentGateways\FreedomPay\FreedomPay;

return [
    'default' => FreedomPay::class,
];
