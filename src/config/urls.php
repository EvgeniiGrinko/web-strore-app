<?php

declare(strict_types=1);

return [
    'freedom' => [
        'init'   => env('FREEDOMPAY_INIT_URL', 'https://customer.paybox.money/init_payment.php'),
        'payout' => env('FREEDOMPAY_PAYOUT_URL', 'https://customer.paybox.money/api/reg2nonreg'),
    ],
];
